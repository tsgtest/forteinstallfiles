<?xml version="1.0"?>
<xs:schema targetNamespace="urn-legalmacpac-data/10" elementFormDefault="qualified" xmlns:mp10="urn-legalmacpac-data/10" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:attributeGroup name="BaseIDAttributes">
    <!-- applies to segments and variables only -->
    <xs:attribute name="TagID" type="xs:string">
      <xs:annotation>
        <xs:documentation>tag identifier that is unique within segment.  
			Has form DocID.IndexedSegmentName.IndexedNestedSegmentName.IndexedNestedNestedSegmentName(etc).IndexedVariableName - 
			e.g. Doc1234.EnglishLetter1.LetterSignature1.FirmName1
		  </xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="ObjectData" type="xs:string">
      <xs:annotation>
        <xs:documentation>if tag represents an mpObject, encrypted xml string containing values of doc props which don't 
			directly correspond to text in the document; if tag represents an unsaved user variable (VariableID = generic), 
			variable info, e.g. control type; otherwise, not present.  Data is stored as pipe-delimited name/value pairs
			e.g. Name1=Value1|Name2=Value2
		  </xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:attributeGroup>
  <xs:attributeGroup name="BaseAttributes">
    <xs:attribute name="Tag" type="xs:string">
      <xs:annotation>
        <xs:documentation>reserved for third-party use</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="TransformParameters" type="xs:string">
      <xs:annotation>
        <xs:documentation>used for transformation to e-filing or other third-party schema</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="Reserved" type="xs:string">
      <xs:annotation>
        <xs:documentation>reserved for MacPac use</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="TempID" type="xs:string">
      <xs:annotation>
        <xs:documentation>used at runtime to uniquely identify a tag</xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:attributeGroup>
  <xs:complexType name="SubVariable" mixed="true">
    <xs:annotation>
      <xs:documentation>defines a portion of an mp10:Variable</xs:documentation>
    </xs:annotation>
    <xs:choice minOccurs="0" maxOccurs="unbounded">
      <xs:element name="mSubVar" type="mp10:SubVariable" />
    </xs:choice>
    <xs:attribute name="Name" type="xs:string">
      <xs:annotation>
        <xs:documentation>Name of the sub variable - for use by mp controls and third parties</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="ObjectData" type="xs:string">
      <xs:annotation>
        <xs:documentation>Contains name/value pairs associated with the subvariable</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attributeGroup ref="mp10:BaseAttributes" />
  </xs:complexType>
  <xs:complexType name="Block" mixed="true">
    <xs:annotation>
      <xs:documentation>Defines a document block that is not a segment nor a variable</xs:documentation>
    </xs:annotation>
    <xs:choice minOccurs="0" maxOccurs="unbounded">
      <xs:element name="mVar" type="mp10:Variable" />
      <xs:element name="mSEG" type="mp10:Segment" />
      <xs:element name="mDel" type="mp10:DeletedBlock" />
    </xs:choice>
    <xs:attributeGroup ref="mp10:BaseAttributes" />
    <xs:attributeGroup ref="mp10:BaseIDAttributes" />
  </xs:complexType>
  <xs:complexType name="DeletedBlock" mixed="false">
    <xs:annotation>
      <xs:documentation>Defines a deleted block</xs:documentation>
    </xs:annotation>
    <xs:attributeGroup ref="mp10:BaseAttributes" />
    <xs:attributeGroup ref="mp10:BaseIDAttributes" />
  </xs:complexType>
  <xs:complexType name="SectionProperties" mixed="false">
    <xs:annotation>
      <xs:documentation>Defines properties applicable to a section of a Word document</xs:documentation>
    </xs:annotation>
    <xs:attributeGroup ref="mp10:BaseAttributes" />
    <xs:attributeGroup ref="mp10:BaseIDAttributes" />
  </xs:complexType>
  <xs:complexType name="DocumentProperties" mixed="false">
    <xs:annotation>
      <xs:documentation>Defines properties applicable to a Word document</xs:documentation>
    </xs:annotation>
    <xs:attributeGroup ref="mp10:BaseAttributes" />
    <xs:attributeGroup ref="mp10:BaseIDAttributes" />
  </xs:complexType>
  <xs:complexType name="Variable" mixed="true">
    <xs:annotation>
      <xs:documentation>defines the WordML of a MacPac variable</xs:documentation>
    </xs:annotation>
    <xs:choice minOccurs="0" maxOccurs="unbounded">
      <xs:element name="mSubVar" type="mp10:SubVariable" />
      <xs:element name="mDel" type="mp10:DeletedBlock" />
    </xs:choice>
    <xs:attributeGroup ref="mp10:BaseAttributes" />
    <xs:attributeGroup ref="mp10:BaseIDAttributes" />
  </xs:complexType>
  <xs:complexType name="Segment" mixed="true">
    <xs:annotation>
      <xs:documentation>defines the WordML of a MacPac segment</xs:documentation>
      <xs:documentation>ObjectData contains 1)VariableDefinitions 2)SegmentActionDefinitions
				3)SegmentTypeID 4)ObjectTypeID 5)Pleading levels 0-4</xs:documentation>
    </xs:annotation>
    <xs:choice minOccurs="0" maxOccurs="unbounded">
      <xs:element name="mSEG" type="mp10:Segment" />
      <xs:element name="mBlock" type="mp10:Block" />
      <xs:element name="mVar" type="mp10:Variable" />
      <xs:element name="mDel" type="mp10:DeletedBlock" />
      <xs:element name="mSecProps" type="mp10:SectionProperties" />
      <xs:element name="mDocProps" type="mp10:DocumentProperties" />
    </xs:choice>
    <xs:attribute name="Authors" type="xs:string">
      <xs:annotation>
        <xs:documentation>encrypted xml string containing author info; present only if element represents an mpObject</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="DeletedScopes" type="xs:string">
      <xs:annotation>
        <xs:documentation>encrypted xml string containing the WordML for variables with expanded deleted scopes that are currently in their deleted state</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="PartNumber" type="xs:string">
      <xs:annotation>
        <xs:documentation>uniquely identifies each of the individual mSEGs of which a segment is comprised</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="AssociatedParentVariable" type="xs:string">
      <xs:annotation>
        <xs:documentation>ID of variable whose InsertSegment action controls the segment</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="VersionTimestamp" type="xs:dateTime">
      <xs:annotation>
        <xs:documentation>datetime that this version of the segment was edited - if present and there is a more recent version, the newer version will be inserted</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attributeGroup ref="mp10:BaseAttributes" />
    <xs:attributeGroup ref="mp10:BaseIDAttributes" />
  </xs:complexType>
  <xs:element name="mSEG" type="mp10:Segment" />
  <xs:element name="mBlock" type="mp10:Block" />
  <xs:element name="mVar" type="mp10:Variable" />
  <xs:element name="mDel" type="mp10:DeletedBlock" />
  <xs:element name="mSecProps" type="mp10:SectionProperties" />
  <xs:element name="mDocProps" type="mp10:DocumentProperties" />
</xs:schema>