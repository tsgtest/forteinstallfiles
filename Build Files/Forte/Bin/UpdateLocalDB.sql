1.CREATE TABLE SyncPeopleTmp;;
2.DROP PROCEDURE spSyncPeopleUpdate;;
3.CREATE PROCEDURE spSyncPeopleUpdate AS UPDATE People AS p INNER JOIN SyncPeopleTmp AS t ON (p.ID1=t.ID1) AND (p.ID2=t.ID2) SET p.OwnerID = t.OwnerID, p.UsageState = t.UsageState, p.UserID = t.UserID, p.DisplayName = t.DisplayName, p.LinkedPersonID = t.LinkedPersonID, p.Prefix = t.Prefix, p.LastName = t.LastName, p.FirstName = t.FirstName, p.MI = t.MI, p.Suffix = t.Suffix, p.FullName = t.FullName, p.ShortName = t.ShortName, p.OfficeID = t.OfficeID, p.Initials = t.Initials, p.IsAttorney = t.IsAttorney, p.IsAuthor = t.IsAuthor, p.Title = t.Title, p.Phone = t.Phone, p.Fax = t.Fax, p.EMail = t.EMail, p.AdmittedIn = t.AdmittedIn, p.LastEditTime = t.LastEditTime  WHERE t.LastEditTime>p.LastEditTime;;
4.DROP TABLE SyncPeopleTmp;;

5.DROP PROCEDURE spJurisdictions0;;
6.CREATE PROCEDURE spJurisdictions0 AS SELECT ID, Name FROM Jurisdictions0 ORDER BY Name;;

7.DROP PROCEDURE spJurisdictions1;;
8.CREATE PROCEDURE spJurisdictions1 AS PARAMETERS ParentID Long; SELECT l.ID, l.Name, l.Parent FROM Jurisdictions1 AS l WHERE l.Parent=[ParentID] Or [ParentID]=0 ORDER BY Name;;

9.DROP PROCEDURE spJurisdictions2;;
10.CREATE PROCEDURE spJurisdictions2 AS PARAMETERS ParentID Long; SELECT l.ID, l.Name, l.Parent FROM Jurisdictions2 AS l WHERE l.Parent=[ParentID] Or [ParentID]=0 ORDER BY Name;;

11.DROP PROCEDURE spJurisdictions3;;
12.CREATE PROCEDURE spJurisdictions3 AS PARAMETERS ParentID Long; SELECT l.ID, l.Name, l.Parent FROM Jurisdictions3 AS l WHERE l.Parent=[ParentID] Or [ParentID]=0 ORDER BY Name;;

13.DROP PROCEDURE spJurisdictions4;;
14.CREATE PROCEDURE spJurisdictions4 AS PARAMETERS ParentID Long; SELECT l.ID, l.Name, l.Parent FROM Jurisdictions4 AS l WHERE l.Parent=[ParentID] Or [ParentID]=0 ORDER BY Name;;

15.ALTER TABLE Keysets ADD COLUMN Culture Long NOT NULL;;
16.UPDATE Keysets SET Culture=1033;;
17.ALTER TABLE Keysets ADD COLUMN LastEditTimeTemp DateTime NOT NULL;;
18.UPDATE Keysets SET LastEditTimeTemp = LastEditTime;;
19.ALTER TABLE Keysets DROP COLUMN LastEditTime;;
20.ALTER TABLE Keysets DROP CONSTRAINT PRIMARYKEY;;
21.ALTER TABLE Keysets ADD CONSTRAINT PrimaryKey PRIMARY KEY(ScopeID1, ScopeID2, OwnerID1, OwnerID2, Type, Culture);;
22.ALTER TABLE Keysets ADD COLUMN LastEditTime DateTime NOT NULL;;
23.UPDATE Keysets SET LastEditTime = LastEditTimeTemp;;
24.ALTER TABLE Keysets DROP COLUMN LastEditTimeTemp;;

25.DROP PROCEDURE spKeySetAdd;;
26.CREATE PROCEDURE spKeySetAdd AS PARAMETERS ScopeID1 Long, ScopeID2 Long, OwnerID1 Long, OwnerID2 Long, Type Byte, Culture Long, [Values] Text ( 255 ), LastEditTime DateTime;
INSERT INTO KeySets ( ScopeID1, ScopeID2, OwnerID1, OwnerID2, Type, Culture, [Values], LastEditTime )
SELECT [ScopeID1] AS Expr1, [ScopeID2] AS Expr2, [OwnerID1] AS Expr3, [OwnerID2] AS Expr4, [Type] AS Expr5, [Culture] AS Expr6, [Values] AS Expr7, [LastEditTime];;

27.DROP PROCEDURE spKeySetsCopyForObjectID;;
28.CREATE PROCEDURE spKeySetsCopyForObjectID AS PARAMETERS ObjectID1 Long, NewObjectID1 Long, OwnerID1 Long, OwnerID2 Long, LastEditTime DateTime; INSERT INTO KeySets ( ScopeID1, ScopeID2, OwnerID1, OwnerID2, Type, Culture, [Values], UsageState, LastEditTime ) SELECT [NewObjectID1] AS Expr2, KeySets.ScopeID2, KeySets.OwnerID1, KeySets.OwnerID2, KeySets.Type, KeySets.Culture, KeySets.Values, KeySets.UsageState, [LastEditTime] FROM KeySets WHERE (KeySets.ScopeID1=[ObjectID1]) AND ((KeySets.OwnerID1=[OwnerID1] AND KeySets.OwnerID2=[OwnerID2]) OR ([OwnerID1] Is Null And [OwnerID2] is Null));;

29.DROP PROCEDURE spKeySetDelete;;
30.CREATE PROCEDURE spKeySetDelete AS PARAMETERS ScopeID1 Long, ScopeID2 Long, OwnerID1 Long, OwnerID2 Long, Type Byte, Culture Long; DELETE KeySets.* FROM KeySets AS k WHERE (k.ScopeID1=[ScopeID1] And k.ScopeID2=[ScopeID2] And k.OwnerID1=[OwnerID1] And k.OwnerID2=[OwnerID2] And k.Type=[Type] And k.Culture=[Culture]);;

31.DROP PROCEDURE spKeySetSelect;;
32.CREATE PROCEDURE spKeySetSelect AS PARAMETERS ScopeID Long, OwnerID1 Long, OwnerID2 Long, Type Byte, Culture Long; SELECT TOP 1 k.ScopeID, k.OwnerID1, k.OwnerID2, k.Type, k.Culture, k.Values FROM KeySets AS k RIGHT JOIN (SELECT ID1, ID2, 4 as SortIndex FROM People as p WHERE p.ID1=[OwnerID1] AND p.ID2=[OwnerID2] UNION SELECT o.OfficeID as ID1, -999999999 as ID2, 1 as SortIndex FROM People as o WHERE o.ID1=[OwnerID1] And o.ID2=[OwnerID2] UNION SELECT g.GroupID AS ID1, -999999999 AS ID2, 2 AS SortIndex FROM GroupAssignments AS g WHERE g.PersonID=[OwnerID1] AND [OwnerID2]=0 UNION SELECT -999999999 as ID1, -999999999 as ID2, 0 as SortIndex FROM Reserved UNION SELECT [OwnerID1] as ID1, [OwnerID2] as ID2, 3 as SortIndex FROM Reserved)  AS p ON (p.ID2=k.OwnerID2) AND (p.ID1=k.OwnerID1) WHERE (k.ScopeID=[ScopeID] And k.Type=[Type] And k.Culture=[Culture]) ORDER BY p.SortIndex DESC;;

33.DROP PROCEDURE spKeySetSelectForAlias;;
34.CREATE PROCEDURE spKeySetSelectForAlias AS PARAMETERS ScopeID1 Long, ScopeID2 Long, PersonID1 Long, PersonID2 Long, OfficeID Long, Type Byte, Culture Long; SELECT TOP 2 k.ScopeID1, k.ScopeID2, k.OwnerID1, k.OwnerID2, k.Type, k.Culture, k.Values FROM KeySets AS k RIGHT JOIN (SELECT [PersonID1] AS ID1, [PersonID2] AS ID2, 4 as SortIndex FROM Reserved UNION SELECT p.LinkedPersonID AS ID1, 0 AS ID2, 3 AS SortIndex FROM People AS p WHERE p.ID1=[PersonID1] AND p.ID2=[PersonID2] UNION SELECT g.GroupID AS ID1, -999999999 AS ID2, 2 AS SortIndex FROM GroupAssignments AS g WHERE g.PersonID=[PersonID1] AND [PersonID2]=0 UNION SELECT [OfficeID] as ID1, -999999999 as ID2, 1 as SortIndex FROM Reserved UNION SELECT -999999999 as ID1, -999999999 as ID2, 0 as SortIndex FROM Reserved)  AS p ON (p.ID2=k.OwnerID2) AND (p.ID1=k.OwnerID1) WHERE (k.ScopeID1=[ScopeID1] And k.ScopeID2=[ScopeID2] And k.Type=[Type] And k.Culture=[Culture]) ORDER BY p.SortIndex DESC;;

35.DROP PROCEDURE spKeySetSelectForPerson;;
36.CREATE PROCEDURE spKeySetSelectForPerson AS PARAMETERS ScopeID1 Long, ScopeID2 Long, PersonID1 Long, PersonID2 Long, OfficeID Long, Type Byte, Culture Long; SELECT TOP 2 k.ScopeID1, k.ScopeID2, k.OwnerID1, k.OwnerID2, k.Type, k.Culture, k.Values FROM KeySets AS k RIGHT JOIN (SELECT [PersonID1] AS ID1, [PersonID2] AS ID2, 3 as SortIndex FROM Reserved UNION SELECT g.GroupID AS ID1, -999999999 AS ID2, 2 AS SortIndex FROM GroupAssignments AS g WHERE g.PersonID=[PersonID1] AND [PersonID2]=0 UNION SELECT [OfficeID] as ID1, -999999999 as ID2, 1 as SortIndex FROM Reserved UNION SELECT -999999999 as ID1, -999999999 as ID2, 0 as SortIndex FROM Reserved)  AS p ON (p.ID1=k.OwnerID1) AND (p.ID2=k.OwnerID2) WHERE (k.ScopeID1=[ScopeID1] And k.ScopeID2=[ScopeID2] And k.Type=[Type] And k.Culture=[Culture]) ORDER BY p.SortIndex DESC;;

37.DROP PROCEDURE spKeySetUpdate;;
38.CREATE PROCEDURE spKeySetUpdate AS PARAMETERS ScopeID1 Long, ScopeID2 Long, OwnerID1 Long, OwnerID2 Long, Type Byte, Culture Long, [Values] Text ( 255 ), LastEditTime DateTime; UPDATE KeySets AS k SET k.[Values] = [Values], k.LastEditTime = [LastEditTime] WHERE (k.ScopeID1=[ScopeID1] And k.ScopeID2=[ScopeID2] And k.OwnerID1=[OwnerID1] And k.OwnerID2=[OwnerID2] And k.Type=[Type] And k.Culture=[Culture]);;

39.CREATE PROCEDURE spMetadataUpdate AS PARAMETERS Name Text ( 255 ), [Value] Text ( 255 ); UPDATE Metadata AS m SET m.[Value] = [Value] WHERE (m.Name=[Name]);;

40.CREATE TABLE SyncKeySetsTmp (ScopeID1 LONG, ScopeID2  LONG, OwnerID1 LONG, OwnerID2  LONG, [Type] BYTE, Culture LONG,  [Values] MEMO, UsageState BYTE, LastEditTime DATETIME, CONSTRAINT multifieldindex  PRIMARY KEY(ScopeID1, ScopeID2, OwnerID1, OwnerID2, [Type], Culture));;
41.DROP PROCEDURE spSyncKeySetsUpdate;;
42.CREATE PROCEDURE spSyncKeySetsUpdate AS UPDATE KeySets AS p INNER JOIN SyncKeySetsTmp AS t ON (p.ScopeID2=t.ScopeID2) AND (p.ScopeID1=t.ScopeID1) AND (p.OwnerID1=t.OwnerID1) AND (p.OwnerID2=t.OwnerID2) AND (p.Type=t.Type) AND (p.Culture=t.Culture) SET p.[Values] = t.Values, p.UsageState = t.UsageState, p.LastEditTime = t.LastEditTime WHERE t.LastEditTime>p.LastEditTime;;

43.DROP PROCEDURE spSyncKeySetsTmpCreate;;
44.CREATE PROCEDURE spSyncKeySetsTmpCreate AS CREATE TABLE SyncKeySetsTmp (ScopeID1 LONG, ScopeID2  LONG, OwnerID1 LONG, OwnerID2  LONG, [Type] BYTE, Culture LONG,  [Values] MEMO, UsageState BYTE, LastEditTime DATETIME, CONSTRAINT multifieldindex  PRIMARY KEY(ScopeID1, ScopeID2, OwnerID1, OwnerID2, [Type], Culture));;

45.DROP PROCEDURE spSyncKeySetsAdd;;
46.CREATE PROCEDURE spSyncKeySetsAdd AS INSERT INTO KeySets ( ScopeID1, ScopeID2, OwnerID1, OwnerID2, Type, Culture, [Values], UsageState, LastEditTime ) SELECT t.ScopeID1, t.ScopeID2, t.OwnerID1, t.OwnerID2, t.Type, t.Culture, t.Values, t.UsageState, t.LastEditTime FROM SyncKeySetsTmp AS t LEFT JOIN KeySets AS p ON (t.Type=p.Type) AND (t.OwnerID2=p.OwnerID2) AND (t.OwnerID1=p.OwnerID1) AND (t.ScopeID1=p.ScopeID1) AND (t.ScopeID2=p.ScopeID2) AND (t.Culture=p.Culture) WHERE p.OwnerID1 Is Null;;

47.DROP PROCEDURE spSyncKeySetsDelete;;
48.CREATE PROCEDURE spSyncKeySetsDelete AS DELETE t.* FROM KeySets AS t LEFT JOIN SyncKeySetsTmp AS s ON (t.Type=s.Type) AND (t.OwnerID2=s.OwnerID2) AND (t.OwnerID1=s.OwnerID1) AND (t.ScopeID1=s.ScopeID1) AND (t.ScopeID2=s.ScopeID2) AND (t.Culture=s.Culture) WHERE (t.ScopeID1 Is Not Null and s.ScopeID2 Is Not Null AND t.OwnerID1 Is Not Null AND s.OwnerID1 Is Not Null AND t.OwnerID2 Is Not Null AND s.OwnerID2 Is Not Null AND t.Type Is Not Null AND s.Type Is Not Null AND t.Culture Is Not Null AND s.Culture Is Not Null);;
49.DROP TABLE SyncKeySetsTmp;;

50.DROP PROCEDURE spUserSegmentsCount;;
51.CREATE PROCEDURE spUserSegmentsCount AS PARAMETERS OwnerID Long; SELECT COUNT(*) AS NumberOfUserSegments FROM UserSegments AS u WHERE u.OwnerID1=[OwnerID];;

52.DROP PROCEDURE spKeySetSelectForGroupOrOffice;;
53.CREATE PROCEDURE spKeySetSelectForGroupOrOffice AS PARAMETERS ScopeID1 Long, ScopeID2 Long, EntityID1 Long, Type Byte, Culture Long; SELECT TOP 2 k.ScopeID1, k.ScopeID2, k.OwnerID1, k.OwnerID2, k.Type, k.Culture, k.Values FROM KeySets AS k WHERE (((k.OwnerID1)=[EntityID1] Or (k.OwnerID1)=-999999999) AND ((k.ScopeID1)=[ScopeID1]) AND ((k.ScopeID2)=[ScopeID2]) AND ((k.OwnerID2)=-999999999) AND ((k.Type)=[Type]) AND ((k.Culture)=[Culture])) ORDER BY k.OwnerID1 DESC;;

54.DROP PROCEDURE spKeySetsUpdateGroups;;
55.CREATE PROCEDURE spKeySetsUpdateGroups AS PARAMETERS ScopeID1 Long, ScopeID2 Long, Type Byte, Culture Long, Name Text ( 255 ), [Value] Text ( 255 ), LastEditTime DateTime; UPDATE KeySets AS k SET k.[Values] = Left$(k.Values,Instr(k.Values,[Name] & "�")+Len([Name] & "�")-1) & [Value] & IIf(Instr(Instr(k.Values,[Name] & "�")+1,k.Values,"|")>0,Mid$(k.Values,Instr(Instr(k.Values,[Name] & "�")+1,k.Values,"|")),""), k.LastEditTime = [LastEditTime] WHERE k.ScopeID=[ScopeID1] and k.ScopeID2=[ScopeID2] And k.Type=[Type] And k.Culture=[Culture] And k.OwnerID1<0 And Instr(k.Values,[Name] & "�")<>0;;

56.CREATE PROCEDURE spUserSegmentsNameExists AS PARAMETERS OwnerID Long, Name Text ( 255 ); SELECT Count(*) FROM UserSegments AS v WHERE (v.OwnerID1=[OwnerID] or [OwnerID] is Null) AND v.DisplayName Like [Name];;

57.ALTER TABLE Deletions DROP CONSTRAINT PRIMARYKEY;;
58.DELETE * FROM Deletions WHERE (ObjectTypeID Is Null OR ObjectID Is Null);;
59.ALTER TABLE Deletions ADD CONSTRAINT PrimaryKey PRIMARY KEY(ID, ObjectTypeID, ObjectID);;

60.DROP PROCEDURE spSyncDeletionsTmpCreate;;
61.CREATE PROCEDURE spSyncDeletionsTmpCreate AS CREATE TABLE SyncDeletionsTmp (ID  INT, ObjectTypeID  LONG, ObjectID TEXT(255),  LastEditTime DATETIME, CONSTRAINT index1  PRIMARY KEY(ID, ObjectTypeID, ObjectID));;

62.DROP TABLE SyncDeletionsTmp;;
63.CREATE TABLE SyncDeletionsTmp (ID  INT, ObjectTypeID  LONG, ObjectID TEXT(255),  LastEditTime DATETIME);;

64.DROP PROCEDURE spSyncDeletionsAdd;;
65.CREATE PROCEDURE spSyncDeletionsAdd AS INSERT INTO Deletions SELECT t.* FROM SyncDeletionsTmp AS t LEFT JOIN Deletions AS p ON (t.ID=p.ID AND t.ObjectTypeID=p.ObjectTypeID AND t.ObjectID=p.ObjectID) WHERE p.ID Is Null;;

66.DROP PROCEDURE spSyncDeletionsUpdate;;
67.CREATE PROCEDURE spSyncDeletionsUpdate AS UPDATE Deletions AS a INNER JOIN SyncDeletionsTmp AS s ON (a.ID=s.ID AND a.ObjectTypeID=s.ObjectTypeID AND a.ObjectID=s.ObjectID) SET a.LastEditTime = s.LastEditTime WHERE s.LastEditTime>a.LastEditTime;;

68.DROP TABLE SyncDeletionsTmp;;

69.DROP PROCEDURE spPermissionsDeleteByObjectIDs;;
70.CREATE PROCEDURE spPermissionsDeleteByObjectIDs AS PARAMETERS ObjectTypeID Long, ObjectID1 Long, ObjectID2 Long, PersonID Long; DELETE p.* FROM Permissions AS p WHERE (p.ObjectTypeID=[ObjectTypeID]) AND (p.ObjectID1=[ObjectID1]) AND (p.ObjectID2=[ObjectID2]) AND (p.PersonID=[PersonID] OR [PersonID] Is Null);;

71.ALTER TABLE People ADD COLUMN DefaultOfficeRecordID1 Long;;
72.ALTER TABLE People ADD COLUMN OfficeUsageState Byte;;

73.DROP PROCEDURE spUserAuthorPreferencePermissions;;
74.CREATE PROCEDURE spUserAuthorPreferencePermissions AS PARAMETERS UserID Long; SELECT peop.ID AS ID, peop.DisplayName & '  (' & OfficeDisplayName & ')' AS DisplayName FROM (SELECT DISTINCT ID1 & '.' & ID2 AS ID, DisplayName, OfficeID, LinkedPersonID FROM People AS p WHERE (p.ID1=[UserID] OR p.LinkedPersonID=[UserID] OR OwnerID=[UserID]) AND p.IsAuthor=-1 AND p.UsageState<>0) AS peop INNER JOIN (SELECT ID, DisplayName AS OfficeDisplayName FROM Offices) AS o ON peop.OfficeID=o.ID ORDER BY DisplayName;;

75.DROP PROCEDURE spSyncPeopleTmpCreate;;
76.CREATE PROCEDURE spSyncPeopleTmpCreate AS CREATE TABLE SyncPeopleTmp (ID1 LONG, ID2 LONG, OwnerID LONG, UsageState  BYTE, UserID TEXT(255), DisplayName  TEXT(255),  LinkedPersonID LONG, Prefix TEXT(255), LastName TEXT(255), FirstName TEXT(255), MI TEXT(255), Suffix TEXT(255), FullName TEXT(255), ShortName TEXT(255), OfficeID LONG, Initials TEXT(255), IsAttorney YESNO, IsAuthor YESNO, Title TEXT(255), Phone TEXT(255), Fax TEXT(255), EMail TEXT(255), AdmittedIn TEXT(255), DefaultOfficeRecordID1 LONG, OfficeUsageState BYTE,  LastEditTime DATETIME, CONSTRAINT multifieldindex  PRIMARY KEY(ID1, ID2));;
77.UPDATE Segments SET Segments.LastEditTime = Now() WHERE (([ID]=-1 Or [ID]=-2 Or [ID]=-3));;

78.CREATE TABLE SyncPeopleTmp;;
79.DROP PROCEDURE spSyncPeopleUpdate;;
80.CREATE PROCEDURE spSyncPeopleUpdate AS UPDATE People AS p INNER JOIN SyncPeopleTmp AS t ON (p.ID2 = t.ID2) AND (p.ID1 = t.ID1) SET p.OwnerID = t.OwnerID, p.UsageState = t.UsageState, p.UserID = t.UserID, p.DisplayName = t.DisplayName, p.LinkedPersonID = t.LinkedPersonID, p.Prefix = t.Prefix, p.LastName = t.LastName, p.FirstName = t.FirstName, p.MI = t.MI, p.Suffix = t.Suffix, p.FullName = t.FullName, p.ShortName = t.ShortName, p.OfficeID = t.OfficeID, p.Initials = t.Initials, p.IsAttorney = t.IsAttorney, p.IsAuthor = t.IsAuthor, p.Title = t.Title, p.Phone = t.Phone, p.Fax = t.Fax, p.EMail = t.EMail, p.AdmittedIn = t.AdmittedIn, p.DefaultOfficeRecordID1=t.DefaultOfficeRecordID1, p.OfficeUsageState=t.OfficeUsageState,p.LastEditTime = t.LastEditTime 
WHERE (((t.LastEditTime)>[p].[LastEditTime]));;
81.DROP TABLE SyncPeopleTmp;;

82.DROP PROCEDURE spPeopleGroupPersonIsMember;;
83.CREATE PROCEDURE spPeopleGroupPersonIsMember AS PARAMETERS ID1 Long, ID2 Long, GroupID Long; SELECT Count(g.PersonID)>0 AS Expr1 FROM GroupAssignments AS g INNER JOIN People p ON g.PersonID=p.ID1 AND g.PersonID2=p.ID2 WHERE g.PersonID=[ID1] And g.PersonID2=[ID2] And g.GroupID=[GroupID] AND p.UsageState=1;;

84.DROP PROCEDURE spPeopleGroupMembersCount;;
85.CREATE PROCEDURE spPeopleGroupMembersCount AS PARAMETERS GroupID Long; SELECT COUNT(*) FROM GroupAssignments AS g INNER JOIN People p ON g.PersonID=p.ID1 AND g.PersonID2=p.ID2 WHERE g.GroupID=[GroupID] AND p.UsageState=1;;

86.ALTER TABLE People ADD COLUMN LinkedExternally YesNo;;

87.DROP PROCEDURE spFolderMembersForDisplay;;
88.CREATE PROCEDURE spFolderMembersForDisplay AS PARAMETERS FolderID Long;
SELECT s.DisplayName AS Name, [FolderID] & '.' & s.ID AS ID, s.IntendedUse, instr(s.XML, "<mAuthorPrefs>") > 0 AS ContainsAuthorPreferences FROM Segments AS s INNER JOIN (SELECT AssignedObjectID FROM Assignments WHERE AssignedObjectTypeID=100 AND TargetObjectID=[FolderID] AND TargetObjectTypeID=101) AS oa ON s.ID=oa.AssignedObjectID ORDER BY s.DisplayName;;

89.DROP PROCEDURE spFolderMembersForDisplayTranslated;;
90.CREATE PROCEDURE spFolderMembersForDisplayTranslated AS PARAMETERS FolderID Long, Locale Long;
SELECT IIF(t.Value1 Is Null, notT.DisplayName, t.Value1) AS Name, [FolderID] & '.' & s.ID AS ID, s.IntendedUse, instr(s.XML,"<mAuthorPrefs>") > 0 AS ContainsAuthorPreferences FROM (SELECT * FROM Segments AS s INNER JOIN (SELECT AssignedObjectID FROM Assignments WHERE AssignedObjectTypeID=100 AND TargetObjectID=[FolderID] AND TargetObjectTypeID=101) AS oa ON s.ID=oa.AssignedObjectID) AS notT LEFT JOIN (SELECT ID, Value1 FROM Translations WHERE LocaleID=[Locale]) AS t ON notT.TranslationID= t.ID ORDER BY notT.Name;;

91.DROP PROCEDURE spSegments;;
92.CREATE PROCEDURE spSegments AS PARAMETERS TypeID Long, L0 Long, L1 Long, L2 Long, L3 Long, L4 Long; SELECT s.ID, s.DisplayName, s.Name, s.TranslationID, s.TypeID, s.IntendedUse, s.XML, s.L0, s.L1, s.L2, s.L3, s.L4, s.MenuInsertionOptions, s.DefaultMenuInsertionBehavior, s.DefaultDragLocation, s.DefaultDragBehavior, s.DefaultDoubleClickLocation, s.DefaultDoubleClickBehavior, s.HelpText, s.ChildSegmentIDs, s.LastEditTime FROM Segments AS s WHERE (s.TypeID=[TypeID] Or [TypeID]=0) And (s.L0=[L0] Or [L0]=0) And (s.L1=[L1] Or [L1]=0) And (s.L2=[L2] Or [L2]=0) And (s.L3=[L3] Or [L3]=0) And (s.L4=[L4] Or [L4]=0) ORDER BY s.DisplayName;;

93.DROP PROCEDURE spPersonDelete;;
94.CREATE PROCEDURE spPersonDelete AS  PARAMETERS ID1 Long, ID2 Long, LastEditTime DateTime; UPDATE People AS p SET p.UsageState = 0, p.LastEditTime =[LastEditTime] WHERE (((p.ID1)=[ID1]) And ((p.ID2)=[ID2]));;

95.DELETE * FROM AttyLicenses WHERE ((ID2=0 OR ID2=-999999999) AND OwnerID2=0);;

96. DROP PROCEDURE spUserPermissionsGetPermittedUsers;;
97. CREATE PROCEDURE spUserPermissionsGetPermittedUsers AS PARAMETERS ObjectTypeID Long, ObjectID1 Long, ObjectID2 Long; SELECT PersonID FROM Permissions AS p1 INNER JOIN People p ON p1.PersonID=p.ID1 AND p1.PersonID2=p.ID2 WHERE p1.ObjectTypeID=[ObjectTypeID] AND p1.ObjectID1=[ObjectID1] AND p1.ObjectID2=[ObjectID2];;

98. DROP PROCEDURE spFolderMembersForDisplay;;
99. CREATE PROCEDURE spFolderMembersForDisplay AS PARAMETERS FolderID Long;SELECT s.DisplayName AS Name, [FolderID] & '.' & s.ID AS ID, s.IntendedUse, InStr(s.XML,"<mAuthorPrefs>")>0 AS ContainsAuthorPreferences, s.TypeID FROM Segments AS s INNER JOIN (SELECT AssignedObjectID FROM Assignments WHERE AssignedObjectTypeID=100 AND TargetObjectID=[FolderID] AND TargetObjectTypeID=101) AS oa ON s.ID = oa.AssignedObjectID ORDER BY s.DisplayName;;

100. DROP PROCEDURE spFolderMembersForDisplayTranslated;;
101. CREATE PROCEDURE spFolderMembersForDisplayTranslated AS PARAMETERS FolderID Long, Locale Long;SELECT IIf(t.Value1 Is Null,notT.DisplayName,t.Value1) AS Name, [FolderID] & '.' & s.ID AS ID, s.IntendedUse AS Expr1, InStr(s.XML,"<mAuthorPrefs>")>0 AS ContainsAuthorPreferences, s.TypeID FROM (SELECT * FROM Segments AS s INNER JOIN (SELECT AssignedObjectID FROM Assignments WHERE AssignedObjectTypeID=100 AND TargetObjectID=[FolderID] AND TargetObjectTypeID=101) AS oa ON s.ID=oa.AssignedObjectID) AS notT LEFT JOIN (SELECT ID, Value1 FROM Translations WHERE LocaleID=[Locale]) AS t ON notT.TranslationID = t.ID ORDER BY notT.Name;;

102.DROP PROCEDURE spPersonIDFromSystemUserID;;
103.CREATE PROCEDURE spPersonIDFromSystemUserID AS PARAMETERS SystemUserID Text ( 255 );SELECT ID1 FROM PEOPLE WHERE UCase(UserID)=UCase([SystemUserID]) AND UsageState=1;;

104.CREATE PROCEDURE spSegmentFolders AS PARAMETERS ID Long;SELECT TargetObjectID FROM Assignments WHERE TargetObjectTypeID=101 AND AssignedObjectTypeID=100 AND AssignedObjectID=[ID];;

105.DROP PROCEDURE spAssignments;;
106.CREATE PROCEDURE spAssignments AS PARAMETERS TargetObjectTypeID Long, TargetObjectID Long, AssignedObjectTypeID Long;SELECT s.ID, s.DisplayName, a.AssignedObjectTypeID FROM Assignments AS a INNER JOIN Segments AS s ON a.AssignedObjectID=s.ID WHERE a.TargetObjectTypeID=[TargetObjectTypeID] And a.TargetObjectID=[TargetObjectID] And iif([AssignedObjectTypeID]=0,a.AssignedObjectTypeID<>0,a.AssignedObjectTypeID=[AssignedObjectTypeID]) ORDER BY s.DisplayName;;

107.DROP PROCEDURE spAssignmentsCount;;
108.CREATE PROCEDURE spAssignmentsCount AS PARAMETERS TargetObjectTypeID Long, TargetObjectID Long, AssignedObjectTypeID Long;SELECT COUNT(*) FROM Assignments AS a INNER JOIN Segments AS s ON a.AssignedObjectID=s.ID WHERE a.TargetObjectTypeID=[TargetObjectTypeID] And a.TargetObjectID=[TargetObjectID] And iif([AssignedObjectTypeID]=0,a.AssignedObjectTypeID<>0,a.AssignedObjectTypeID=[AssignedObjectTypeID]);;

109.DROP PROCEDURE spAssignmentsTranslated;;
110.CREATE PROCEDURE spAssignmentsTranslated AS PARAMETERS TargetObjectTypeID Long, TargetObjectID Long, AssignedObjectTypeID Long, LocaleID Long; SELECT IIF(IsNull(t2.Value1), a2.DisplayName, t2.Value1), a2.ID FROM (SELECT s.DisplayName, s.ID, s.TranslationID FROM Assignments AS a INNER JOIN Segments AS s ON a.AssignedObjectID=s.ID WHERE a.TargetObjectTypeID = [TargetObjectTypeID] AND a.TargetObjectID = [TargetObjectID] AND iif([AssignedObjectTypeID]=0,a.AssignedObjectTypeID<>0, a.AssignedObjectTypeID = [AssignedObjectTypeID])) AS a2 LEFT JOIN (SELECT ID, Value1, Value2, Value3, Value4 FROM Translations AS t WHERE t.[LocaleID]=[LocaleID]) AS t2 ON a2.TranslationID = t2.ID;;

111.DROP PROCEDURE spSegmentsGetContainingSegmentIDs;;
112.CREATE PROCEDURE spSegmentsGetContainingSegmentIDs AS PARAMETERS SegmentID Text ( 255 ); SELECT s.ID FROM Segments AS s WHERE ((s.ChildSegmentIDs=[SegmentID]) Or (s.ChildSegmentIDs Like ([SegmentID] & "|%")) Or (s.ChildSegmentIDs Like ("%|" & [SegmentID] & "|%")) Or (s.ChildSegmentIDs Like ([SegmentID] & "|%")) Or (s.ChildSegmentIDs Like ("%|" & [SegmentID] & "|%")) Or (s.ChildSegmentIDs Like ("%|" & [SegmentID]))) UNION SELECT s.ID1 & "." & s.ID2 FROM UserSegments AS s WHERE ((s.ChildSegmentIDs=[SegmentID]) Or (s.ChildSegmentIDs Like ([SegmentID] & "|%")) Or (s.ChildSegmentIDs Like ("%|" & [SegmentID] & "|%")) Or (s.ChildSegmentIDs Like ([SegmentID] & "|%")) Or (s.ChildSegmentIDs Like ("%|" & [SegmentID] & "|%")) Or (s.ChildSegmentIDs Like ("%|" & [SegmentID])));;

113.CREATE PROCEDURE spUserSegmentNameFromID AS PARAMETERS ID1 Long, ID2 Long;SELECT s.DisplayName FROM UserSegments AS s WHERE s.ID1=[ID1] AND s.ID2=[ID2];;

114.DROP PROCEDURE spSyncSegmentsTmpCreate;;
115.CREATE PROCEDURE spSyncSegmentsTmpCreate AS CREATE TABLE SyncSegmentsTmp (ID LONG, DisplayName TEXT(255), Name TEXT(255), TranslationID LONG, TypeID INTEGER, IntendedUse BYTE, XML MEMO, L0 LONG, L1 LONG, L2 LONG, L3 LONG, L4 LONG, MenuInsertionOptions INTEGER, DefaultMenuInsertionBehavior INTEGER, DefaultDragLocation INTEGER, DefaultDragBehavior INTEGER, DefaultDoubleClickLocation INTEGER, DefaultDoubleClickBehavior INTEGER, HelpText MEMO, ChildSegmentIDs TEXT(255), LastEditTime DATETIME, CONSTRAINT index1  PRIMARY KEY(ID));;

116.DROP PROCEDURE spKeySetsCopyForObjectID;;
117.CREATE PROCEDURE spKeySetsCopyForObjectID AS PARAMETERS ObjectID1 Long, ObjectID2 Long, NewObjectID1 Long, NewObjectID2 Long, OwnerID1 Long, OwnerID2 Long, LastEditTime DateTime; INSERT INTO KeySets ( ScopeID1, ScopeID2, OwnerID1, OwnerID2, Type, Culture, [Values], UsageState, LastEditTime ) SELECT [NewObjectID1] AS ScopeID1, [NewObjectID2] AS ScopeID2, KeySets.OwnerID1, KeySets.OwnerID2, KeySets.Type, KeySets.Culture, KeySets.Values, KeySets.UsageState, [LastEditTime] FROM KeySets WHERE (KeySets.OwnerID1=[OwnerID1] AND KeySets.OwnerID2=[OwnerID2] AND KeySets.ScopeID1=[ObjectID1] AND KeySets.ScopeID2=[ObjectID2]) OR ((KeySets.OwnerID1 Is Null) AND (KeySets.OwnerID2 Is Null) AND (KeySets.ScopeID1=[ObjectID1]) AND KeySets.ScopeID2=[ObjectID2]);;

118.CREATE PROCEDURE spSegmentTypeIDFromID AS PARAMETERS SegmentID Long; SELECT s.TypeID FROM Segments AS s WHERE s.ID=[SegmentID];;

119.DROP PROCEDURE spOwnerFoldersForDisplay;;
120.CREATE PROCEDURE spOwnerFoldersForDisplay AS PARAMETERS PersonID Long;  SELECT DISTINCT p2.DisplayName, p2.ID1 FROM (UserSegments AS u INNER JOIN Permissions AS p1 ON (u.ID2 = p1.ObjectID2) AND (u.ID1 = p1.ObjectID1)) INNER JOIN People AS p2 ON (u.OwnerID2 = p2.ID2) AND (u.OwnerID1 = p2.ID1) WHERE (((p1.PersonID)=[PersonID]) AND ((p1.ObjectTypeID)=99) AND ((p1.PersonID2)=0) AND p2.UsageState=1) ORDER BY p2.DisplayName UNION SELECT DISTINCT p2.DisplayName, p2.ID1 FROM (VariableSets AS v INNER JOIN Permissions AS p1 ON (v.ID2 = p1.ObjectID2) AND (v.ID1 = p1.ObjectID1)) INNER JOIN People AS p2 ON (v.OwnerID2 = p2.ID2) AND (v.OwnerID1 = p2.ID1) WHERE (((p1.PersonID)=[PersonID]) AND ((p1.ObjectTypeID)=104) AND ((p1.PersonID2)=0) AND p2.UsageState=1);;

121.DROP PROCEDURE spUserAuthorPreferencePermissions;;
122.CREATE PROCEDURE spUserAuthorPreferencePermissions AS PARAMETERS UserID Long; SELECT peop.ID AS ID, peop.DisplayName & '  (' & OfficeDisplayName & ')' AS DisplayName FROM (SELECT DISTINCT ID1 & '.' & ID2 AS ID, DisplayName, OfficeID, LinkedPersonID FROM People AS p WHERE ((p.ID1=[UserID] And (p.DefaultOfficeRecordID1=0 Or p.DefaultOfficeRecordID1 Is Null Or p.OfficeUsageState=1)) Or p.LinkedPersonID=[UserID] Or OwnerID=[UserID]) And p.IsAuthor=-1 And p.UsageState<>0)  AS peop INNER JOIN (SELECT ID, DisplayName AS OfficeDisplayName FROM Offices) AS o ON peop.OfficeID=o.ID ORDER BY peop.DisplayName;;

123.DROP PROCEDURE spPeopleDeleteRelatedByID;;
124.CREATE PROCEDURE spPeopleDeleteRelatedByID AS PARAMETERS PersonID Long; DELETE p.* FROM People AS p WHERE (p.OwnerID=[PersonID]) OR ((p.LinkedPersonID=[PersonID]) AND p.ID2 <> 0);;

125.DROP PROCEDURE spOfficeIDsByPersonID;;
126.CREATE PROCEDURE spOfficeIDsByPersonID AS PARAMETERS LinkedPersonID Long; SELECT p.OfficeID FROM People AS p WHERE p.ID1=[LinkedPersonID] And p.ID2=0 and p.UserID Is Not Null And p.UsageState=1 And ((p.DefaultOfficeRecordID1=0 OR p.DefaultOfficeRecordID1 Is Null) OR p.OfficeUsageState=1) ORDER by p.OfficeID DESC UNION SELECT l.OfficeID FROM People as l WHERE l.LinkedPersonID=[LinkedPersonID] And l.ID2=0 And l.UsageState=1;;

127.DROP PROCEDURE spFolderPermissionExists;;
128.CREATE PROCEDURE spFolderPermissionExists AS PARAMETERS FolderID Long, UserID Long; SELECT ID1 FROM People AS p INNER JOIN (SELECT ID FROM Assignments AS a RIGHT JOIN (SELECT ID FROM Offices) AS o ON o.ID=a.TargetObjectID WHERE a.TargetObjectTypeID=201 AND a.AssignedObjectTypeID=101 AND a.AssignedObjectID=[FolderID]) AS ao ON ao.ID=p.OfficeID WHERE p.UsageState=1 AND p.ID1=[UserID] AND p.ID2=0 AND ((p.DefaultOfficeRecordID1=0 OR p.DefaultOfficeRecordID1 Is Null) OR p.OfficeUsageState=1) UNION SELECT ID1 FROM People AS p2 INNER JOIN (SELECT PersonID FROM GroupAssignments as ga INNER JOIN (SELECT ID FROM Assignments AS a RIGHT JOIN (SELECT ID FROM PeopleGroups) AS pg ON pg.ID=a.TargetObjectID WHERE a.TargetObjectTypeID=201 AND a.AssignedObjectTypeID=101 AND a.AssignedObjectID=[FolderID]) AS ag ON ag.ID=ga.GroupID) AS g ON g.PersonID=p2.ID1 WHERE p2.ID1=[UserID] AND p2.ID2=0 AND p2.UsageState=1 UNION SELECT ID1 FROM People AS p INNER JOIN (SELECT ID FROM Assignments AS a RIGHT JOIN (SELECT ID FROM Offices) AS o ON o.ID=a.TargetObjectID WHERE a.TargetObjectTypeID=201 AND a.AssignedObjectTypeID=101 AND a.AssignedObjectID=[FolderID]) AS ao ON ao.ID=p.OfficeID WHERE p.UsageState=1 AND p.LinkedPersonID=[UserID] AND p.ID2=0;;

129.CREATE PROCEDURE spSegmentsAvailableToUser AS PARAMETERS TypeID Long, L0 Long, L1 Long, L2 Long, L3 Long, L4 Long, UserID Long; SELECT f.* FROM (SELECT s.*, a.FolderID FROM segments s INNER JOIN (SELECT TargetObjectID AS FolderID, AssignedObjectID FROM Assignments WHERE TargetObjectTypeID=101 AND AssignedObjectTypeID=100)  AS a ON a.AssignedObjectID=s.ID WHERE s.typeid= [TypeID] AND s.L0=[L0] AND s.L1=[L1] AND s.L2=[L2] AND s.L3=[L3] AND s.L4=[L4])  AS f INNER JOIN (SELECT ao.AssignedObjectID AS FolderID FROM People AS p INNER JOIN (SELECT ID, AssignedObjectID FROM Assignments AS a RIGHT JOIN Offices AS o ON o.ID=a.TargetObjectID WHERE a.TargetObjectTypeID=201 AND a.AssignedObjectTypeID=101) AS ao ON ao.ID=p.OfficeID WHERE p.UsageState=1 AND p.ID1=[UserID] AND p.ID2=0 AND ((p.DefaultOfficeRecordID1=0 OR p.DefaultOfficeRecordID1 Is Null) OR p.OfficeUsageState=1) UNION SELECT g.AssignedObjectID FROM People AS p2 INNER JOIN (SELECT * FROM GroupAssignments as ga INNER JOIN (SELECT * FROM Assignments AS a RIGHT JOIN PeopleGroups AS pg ON pg.ID=a.TargetObjectID WHERE a.TargetObjectTypeID=201 AND a.AssignedObjectTypeID=101) AS ag ON ag.ID=ga.GroupID) AS g ON g.PersonID=p2.ID1 WHERE p2.ID1=[UserID] AND p2.ID2=0 AND p2.UsageState=1 UNION SELECT ao.ID FROM People AS p INNER JOIN (SELECT ID FROM Assignments AS a RIGHT JOIN Offices AS o ON o.ID=a.TargetObjectID WHERE a.TargetObjectTypeID=201 AND a.AssignedObjectTypeID=101) AS ao ON ao.ID=p.OfficeID WHERE p.UsageState=1 AND p.LinkedPersonID=[UserID] AND p.ID2=0 UNION SELECT AssignedObjectID FROM Assignments WHERE AssignedObjectTypeID = 101 AND TargetObjectTypeID=201 AND TargetObjectID=-999999999)  AS au ON f.FolderID=au.FolderID;;

130.DROP TABLE SyncDeletionsTmp;;
131.CREATE TABLE SyncDeletionsTmp (ID  INT, ObjectTypeID  LONG, ObjectID TEXT(255),  LastEditTime DATETIME);;
132.CREATE PROCEDURE spSyncDeletionsDeleteDupes AS DELETE s.* FROM SyncDeletionsTmp AS s INNER JOIN Deletions AS a ON (a.ID=s.ID) AND (a.ObjectTypeID=s.ObjectTypeID) AND (a.ObjectID=s.ObjectID);;
133.DROP TABLE SyncDeletionsTmp;;

134.DROP PROCEDURE spSegmentsAvailableToUser;;
135.CREATE PROCEDURE spSegmentsAvailableToUser AS PARAMETERS TypeID Long, L0 Long, L1 Long, L2 Long, L3 Long, L4 Long, UserID Long; SELECT f.* FROM (SELECT s.*, a.FolderID FROM segments AS s INNER JOIN (SELECT TargetObjectID AS FolderID, AssignedObjectID FROM Assignments WHERE TargetObjectTypeID=101 AND AssignedObjectTypeID=100)  AS a ON a.AssignedObjectID=s.ID WHERE s.typeid= [TypeID] AND s.L0=[L0] AND s.L1=[L1] AND s.L2=[L2] AND s.L3=[L3] AND s.L4=[L4])  AS f INNER JOIN (SELECT ao.AssignedObjectID AS FolderID FROM People AS p INNER JOIN (SELECT ID, AssignedObjectID FROM Assignments AS a RIGHT JOIN Offices AS o ON o.ID=a.TargetObjectID WHERE a.TargetObjectTypeID=201 AND a.AssignedObjectTypeID=101) AS ao ON ao.ID=p.OfficeID WHERE p.UsageState=1 AND p.ID1=[UserID] AND p.ID2=0 AND ((p.DefaultOfficeRecordID1=0 OR p.DefaultOfficeRecordID1 Is Null) OR p.OfficeUsageState=1) UNION SELECT g.AssignedObjectID FROM People AS p2 INNER JOIN (SELECT * FROM GroupAssignments as ga INNER JOIN (SELECT * FROM Assignments AS a RIGHT JOIN PeopleGroups AS pg ON pg.ID=a.TargetObjectID WHERE a.TargetObjectTypeID=201 AND a.AssignedObjectTypeID=101) AS ag ON ag.ID=ga.GroupID) AS g ON g.PersonID=p2.ID1 WHERE p2.ID1=[UserID] AND p2.ID2=0 AND p2.UsageState=1 UNION SELECT ao.ID FROM People AS p INNER JOIN (SELECT ID FROM Assignments AS a RIGHT JOIN Offices AS o ON o.ID=a.TargetObjectID WHERE a.TargetObjectTypeID=201 AND a.AssignedObjectTypeID=101) AS ao ON ao.ID=p.OfficeID WHERE p.UsageState=1 AND p.LinkedPersonID=[UserID] AND p.ID2=0 UNION SELECT AssignedObjectID FROM Assignments WHERE AssignedObjectTypeID = 101 AND TargetObjectTypeID=201 AND TargetObjectID=-999999999)  AS au ON f.FolderID=au.FolderID ORDER BY f.DisplayName;;

136.DROP PROCEDURE spUserFolderMembersForDisplay;;
137.CREATE PROCEDURE spUserFolderMembersForDisplay AS PARAMETERS FolderID1 Long, FolderID2 Long; SELECT s.DisplayName AS Name, u.ID1 & "." & u.ID2 AS ID, u.TargetObjectTypeID as MemberType, s.IntendedUse FROM UserFolderMembers AS u INNER JOIN UserSegments AS s ON (u.TargetObjectID1 = s.ID1) AND (u.TargetObjectID2 =s.ID2) WHERE ((u.TargetObjectTypeID=99 or u.TargetObjectTypeID=96) And ((u.UserFolderID1)=[FolderID1]) AND ((u.UserFolderID2)=[FolderID2])) ORDER BY s.DisplayName UNION SELECT s2.DisplayName AS Name, u2.ID1 & "." & u2.ID2 AS ID, u2.TargetObjectTypeID as MemberType, s2.IntendedUse FROM UserFolderMembers AS u2 INNER JOIN Segments AS s2 ON (u2.TargetObjectID1 = s2.ID) WHERE (u2.TargetObjectTypeID=100 And (u2.UserFolderID1=[FolderID1]) AND (u2.UserFolderID2=[FolderID2]) AND (u2.TargetObjectID2=0)) UNION SELECT s3.Name AS Name, u3.ID1 & "." & u3.ID2 AS ID, u3.TargetObjectTypeID as MemberType, 0 FROM UserFolderMembers AS u3 INNER JOIN VariableSets AS s3 ON (u3.TargetObjectID1 = s3.ID1) AND (u3.TargetObjectID2 =s3.ID2) WHERE (u3.TargetObjectTypeID=104 And ((u3.UserFolderID1)=[FolderID1]) AND ((u3.UserFolderID2)=[FolderID2]));;


138.DROP PROCEDURE spUserFolderMembersItemFromID;;
139.CREATE PROCEDURE spUserFolderMembersItemFromID AS PARAMETERS Folder1 Long, Folder2 Long, ID1 Long, ID2 Long; SELECT [ID1] & "." & [ID2] AS ID, u.TargetObjectTypeID, u.TargetObjectID1, u.TargetObjectID2, s.DisplayName AS Name, u.LastEditTime FROM UserFolderMembers AS u INNER JOIN UserSegments AS s ON (u.TargetObjectID1=s.ID1) AND (u.TargetObjectID2=s.ID2) WHERE ((u.TargetObjectTypeID=99 OR u.TargetObjectTypeID=96) And ((u.UserFolderID1)=[Folder1]) And ((u.UserFolderID2)=[Folder2]) And ((u.ID1)=[ID1]) And ((u.ID2)=[ID2])) UNION SELECT [ID1] & "." & [ID2] AS ID, u2.TargetObjectTypeID,  u2.TargetObjectID1, u2.TargetObjectID2, s2.DisplayName AS Name, u2.LastEditTime FROM UserFolderMembers AS u2 INNER JOIN Segments AS s2 ON (u2.TargetObjectID1=s2.ID) WHERE (u2.TargetObjectTypeID=100 And (u2.UserFolderID1=[Folder1]) And (u2.UserFolderID2=[Folder2]) And (u2.ID1=[ID1]) And (u2.ID2=[ID2]) AND (u2.TargetObjectID2=0)) UNION SELECT [ID1] & "." & [ID2] AS ID, u3.TargetObjectTypeID, u3.TargetObjectID1, u3.TargetObjectID2, s3.Name AS Name, u3.LastEditTime FROM UserFolderMembers AS u3 INNER JOIN VariableSets AS s3 ON (u3.TargetObjectID1=s3.ID1) AND (u3.TargetObjectID2=s3.ID2) WHERE (u3.TargetObjectTypeID=104 And ((u3.UserFolderID1)=[Folder1]) And ((u3.UserFolderID2)=[Folder2]) And ((u3.ID1)=[ID1]) And ((u3.ID2)=[ID2]));;

140.DROP PROCEDURE spUserSegmentsByPermissionItemFromID;;
141.CREATE PROCEDURE spUserSegmentsByPermissionItemFromID AS PARAMETERS PersonID Long, ID1 Long, ID2 Long; SELECT us.ID1, us.ID2, us.OwnerID1, us.DisplayName, us.Name, us.IntendedUse, us.TranslationID, us.TypeID, us.XML, us.L0, us.L1, us.L2, us.L3, us.L4, us.MenuInsertionOptions, us.DefaultMenuInsertionBehavior, us.DefaultDragLocation, us.DefaultDragBehavior, us.DefaultDoubleClickLocation, us.DefaultDoubleClickBehavior, us.HelpText, us.ChildSegmentIDs FROM UserSegments AS us INNER JOIN (SELECT u.ID1, u.ID2 FROM UserSegments AS u INNER JOIN (SELECT p.ObjectID1, p.ObjectID2 FROM Permissions p WHERE p.PersonID = [PersonID] and (ObjectTypeID = 99 OR ObjectTypeID = 96)) AS p ON (u.ID1=p.ObjectID1) AND (u.ID2=p.ObjectID2)  UNION SELECT u.ID1, u.ID2 FROM UserSegments u WHERE u.OwnerID1 = [PersonID])  AS SegIDs ON (us.ID1=SegIDs.ID1) AND (us.ID2=SegIDs.ID2) WHERE us.ID1=[ID1] AND us.ID2=[ID2];;

142.DROP PROCEDURE spFolderMembersForDisplay;;
143.CREATE PROCEDURE spFolderMembersForDisplay AS PARAMETERS FolderID Long; SELECT s.DisplayName AS Name, [FolderID] & '.' & s.ID AS ID, s.IntendedUse, InStrB(s.XML,"<mAuthorPrefs>")>0 AS ContainsAuthorPreferences, s.TypeID FROM Segments AS s INNER JOIN (SELECT AssignedObjectID FROM Assignments WHERE AssignedObjectTypeID=100 AND  TargetObjectID=[FolderID] AND TargetObjectTypeID=101)  AS oa ON s.ID = oa.AssignedObjectID ORDER BY s.DisplayName;;

144.DROP PROCEDURE spFolderMembersForDisplayTranslated;;
145.CREATE PROCEDURE spFolderMembersForDisplayTranslated AS PARAMETERS FolderID Long, Locale Long; SELECT IIf(t.Value1 Is Null,notT.DisplayName,t.Value1) AS Name, [FolderID] & '.' & s.ID AS ID, notT.s.IntendedUse AS Expr1, InStrB(s.XML,"<mAuthorPrefs>")>0 AS ContainsAuthorPreferences, notT.s.TypeID FROM (SELECT * FROM Segments AS s INNER JOIN (SELECT AssignedObjectID FROM Assignments WHERE AssignedObjectTypeID=100 AND TargetObjectID=[FolderID] AND TargetObjectTypeID=101) AS oa ON s.ID=oa.AssignedObjectID)  AS notT LEFT JOIN (SELECT ID, Value1 FROM Translations WHERE LocaleID=[Locale])  AS t ON notT.TranslationID = t.ID ORDER BY notT.Name;;

146.CREATE PROCEDURE spAssignmentsParentSegments AS PARAMETERS ChildObjectID Long, ChildObjectTypeID Long; SELECT s.DisplayName, s.ID FROM Segments AS s INNER JOIN (SELECT TargetObjectID FROM Assignments WHERE AssignedObjectID  = [ChildObjectID] AND AssignedObjectTypeID = [ChildObjectTypeID])  AS a ON s.ID = a.TargetObjectID;;

147.DROP PROCEDURE spPersonIDFromSystemUserID;;
148.CREATE PROCEDURE spPersonIDFromSystemUserID AS PARAMETERS SystemUserID Text ( 255 );SELECT ID1 FROM PEOPLE WHERE UCase(UserID)=UCase([SystemUserID]) AND UsageState=1 ORDER BY PEOPLE.ID1;;

149.DROP PROCEDURE spOfficesDelete;;
150.CREATE PROCEDURE spOfficesDelete AS PARAMETERS ID Long, LastEditTime DateTime; UPDATE Offices AS o SET o.UsageState = 0, o.LastEditTime = [LastEditTime] WHERE o.ID=[ID];;

151.CREATE TABLE SyncCountiesTmp;;
152.DROP PROCEDURE spSyncCountiesUpdate;;
153.CREATE PROCEDURE spSyncCountiesUpdate AS UPDATE Counties AS a INNER JOIN SyncCountiesTmp AS s ON a.ID=s.ID SET a.StateID = s.StateID, a.Name = s.Name, a.LastEditTime = s.LastEditTime WHERE s.LastEditTime>a.LastEditTime;;
154.DROP TABLE SyncCountiesTmp;;

155.CREATE TABLE SyncCountriesTmp;;
156.DROP PROCEDURE spSyncCountriesUpdate;;
157.CREATE PROCEDURE spSyncCountriesUpdate AS UPDATE Countries AS a INNER JOIN SyncCountriesTmp AS s ON a.ID=s.ID SET a.ISONum = s.ISONum, a.Name = s.Name, a.LastEditTime = s.LastEditTime WHERE s.LastEditTime>a.LastEditTime;;
158.DROP TABLE SyncCountriesTmp;;

159.CREATE PROCEDURE spCountriesDelete AS PARAMETERS ID Long; DELETE Countries.ID FROM Countries WHERE Countries.ID=[ID];;

160.CREATE PROCEDURE spStatesDelete AS PARAMETERS ID Long; DELETE States.ID FROM States WHERE States.ID=[ID];;

161.CREATE PROCEDURE spCountiesDelete AS PARAMETERS ID Long; DELETE Counties.ID FROM Counties WHERE Counties.ID=[ID];;

162.DROP PROCEDURE spUserPermissionsGetPermittedUsers;;
163.CREATE PROCEDURE spUserPermissionsGetPermittedUsers AS PARAMETERS ObjectTypeID Long, ObjectID1 Long, ObjectID2 Long; SELECT p1.PersonID FROM Permissions AS p1 WHERE (((p1.ObjectTypeID)=[ObjectTypeID]) AND ((p1.ObjectID1)=[ObjectID1]) AND ((p1.ObjectID2)=[ObjectID2]));;

164.DROP TABLE SyncDeletionsTmp;;
165.DROP TABLE SyncDeletionsTmp;;
166.DROP TABLE SyncDeletionsTmp;;
167.DROP TABLE SyncDeletionsTmp;;
168.CREATE TABLE SyncDeletionsTmp (ID  INT, ObjectTypeID  LONG, ObjectID TEXT(255),  LastEditTime DATETIME);;
169.DROP PROCEDURE spSyncDeletionsRemoveNonPeopleRelated;;
170.CREATE PROCEDURE spSyncDeletionsRemoveNonPeopleRelated AS DELETE * FROM SyncDeletionsTmp WHERE ObjectTypeID NOT IN (300,600,201);;
171.DROP TABLE SyncDeletionsTmp;;

172.DROP PROCEDURE spUserSegmentsCountFolderAssignments;;
173.CREATE PROCEDURE spUserSegmentsCountFolderAssignments AS PARAMETERS OwnerID Long, SegmentID1 Long, SegmentID2 Long; SELECT Count(u.ID1) AS CountOfUserSegments FROM UserFolderMembers AS u WHERE ((u.TargetObjectTypeID=99 OR u.TargetObjectTypeID=96) And ((u.TargetObjectID1)=[SegmentID1]) And ((u.TargetObjectID2)=[SegmentID2]) And u.ID1=[OwnerID]);;

174.DROP PROCEDURE spAttyLicenses;;
175.CREATE PROCEDURE spAttyLicenses AS PARAMETERS OwnerID1 Long, OwnerID2 Long; SELECT [ID1] & (IIf([ID2]<>-999999999,"." & Trim(Str([ID2])),"")) AS ID, AttyLicenses.OwnerID1 & (IIf(AttyLicenses.OwnerID2<>-999999999,"." & AttyLicenses.OwnerID2,"")) AS OwnerID, AttyLicenses.Description, AttyLicenses.License, AttyLicenses.L0, AttyLicenses.L1, AttyLicenses.L2, AttyLicenses.L3, AttyLicenses.L4 FROM AttyLicenses WHERE ((AttyLicenses.OwnerID1=[OwnerID1] And AttyLicenses.OwnerID2=[OwnerID2]) Or (AttyLicenses.OwnerID1=[OwnerID1] And [OwnerID2] Is Null) Or ([OwnerID1] Is Null And [OwnerID2] Is Null)) AND (AttyLicenses.UsageState=1);;

176.DROP PROCEDURE spAttyLicensesCount;;
177.CREATE PROCEDURE spAttyLicensesCount AS PARAMETERS OwnerID1 Long, OwnerID2 Long; SELECT COUNT(*) FROM AttyLicenses WHERE (AttyLicenses.UsageState=1) AND ((AttyLicenses.OwnerID1=[OwnerID1] And AttyLicenses.OwnerID2=[OwnerID2]) Or (AttyLicenses.OwnerID1=[OwnerID1] And [OwnerID2] Is Null) Or ([OwnerID1] Is Null And [OwnerID2] Is Null));;

178.DROP PROCEDURE spAttyLicensesByLevels;;
179.CREATE PROCEDURE spAttyLicensesByLevels AS PARAMETERS OwnerID1 Long, OwnerID2 Long, L0 Long, L1 Long, L2 Long, L3 Long, L4 Long; SELECT [ID1] & (IIf([ID2]<>-999999999,"." & Trim(Str([ID2])),"")) AS ID, L.OwnerID1 & (IIf(L.OwnerID2<>-999999999,"." & L.OwnerID2,"")) AS OwnerID, L.Description, L.License, L.L0, L.L1, L.L2, L.L3, L.L4 FROM AttyLicenses AS L WHERE (L.UsageState=1) AND (((L.OwnerID1=[OwnerID1] AND L.OwnerID2=[OwnerID2]) OR (L.OwnerID1=[OwnerID1] AND L.OwnerID2 Is Null) OR (L.OwnerID1 Is Null AND L.OwnerID2 Is Null)) AND ((L.L0=[L0] AND L.L1=[L1] AND L.L2=[L2] AND L.L3=[L3] AND L.L4=[L4]) OR (L.L0=[L0] AND ([L1]=0 OR L.L1=0)) OR (L.L0=[L0] AND L.L1=[L1] AND ([L2]=0 OR L.L2=0)) OR (L.L0=[L0] AND L.L1=[L1] AND L.L2=[L2] AND ([L3]=0 OR L.L3=0)) OR (L.L0=[L0] AND L.L1=[L1] AND L.L2=[L2] AND L.L3=[L3] AND ([L4]=0 OR L.L4=0))));;

180.DROP PROCEDURE spAttyLicensesByLevelsCount;;
181.CREATE PROCEDURE spAttyLicensesByLevelsCount AS PARAMETERS OwnerID1 Long, OwnerID2 Long, L0 Long, L1 Long, L2 Long, L3 Long, L4 Long; SELECT Count(*) AS Expr1 FROM AttyLicenses AS L WHERE (L.UsageState=1) AND (((L.OwnerID1=[OwnerID1] And L.OwnerID2=[OwnerID2]) Or (L.OwnerID1=[OwnerID1] And L.OwnerID2 Is Null) Or (L.OwnerID1 Is Null And L.OwnerID2 Is Null)) And ((L.L0=[L0] And L.L1=[L1] And L.L2=[L2] And L.L3=[L3] And L.L4=[L4]) Or (L.L0=[L0] And ([L1]=0 OR L.L1=0)) Or (L.L0=[L0] And L.L1=[L1] And ([L2]=0 OR L.L2=0)) Or (L.L0=[L0] And L.L1=[L1] And L.L2=[L2] And ([L3]=0 OR L.L3=0)) Or (L.L0=[L0] And L.L1=[L1] And L.L2=[L2] And L.L3=[L3] And ([L4]=0 OR L.L4=0))));;

182.DROP PROCEDURE spFoldersItemFromIDTranslated;;
183.CREATE PROCEDURE spFoldersItemFromIDTranslated AS PARAMETERS Locale Long, FolderID Long; SELECT notT.ID, IIF(t.Value1 Is Null, notT.Name, t.Value1) AS Name, IIF(t.Value2 Is Null, notT.Description, t.Value2) AS Description, f.TranslationID AS TranslationID, f.ParentFolderID, f.Index, f.XML, f.LastEditTime FROM (SELECT f.ID, f.Name, f.Description, f.TranslationID, f.ParentFolderID, f.Index, f.XML, f.LastEditTime FROM Folders AS f)  AS notT LEFT JOIN (SELECT ID, Value1, Value2  FROM Translations WHERE LocaleID=[Locale])  AS t ON notT.TranslationID= t.ID WHERE f.ID = FolderID;;

184.DROP PROCEDURE spAssignmentsItemFromIDTranslated;;
185.CREATE PROCEDURE spAssignmentsItemFromIDTranslated AS PARAMETERS TargetObjectTypeID Long, TargetObjectID Long, AssignedObjectTypeID Long, LocaleID Long, AssignedObjectID Long; SELECT a2.ID, IIF(IsNull(t2.Value1), a2.DisplayName, t2.Value1) AS DisplayName FROM (SELECT s.ID, s.DisplayName, s.TranslationID FROM Assignments AS a INNER JOIN Segments AS s ON a.AssignedObjectID=s.ID WHERE a.TargetObjectTypeID = [TargetObjectTypeID] AND a.TargetObjectID = [TargetObjectID] AND a.AssignedObjectTypeID = [AssignedObjectTypeID] AND a.AssignedObjectID=[AssignedObjectID])  AS a2 LEFT JOIN (SELECT ID, Value1, Value2, Value3, Value4 FROM Translations AS t WHERE t.[LocaleID]=[LocaleID])  AS t2 ON a2.TranslationID = t2.ID;;

186.DROP PROCEDURE spAssignmentsTranslated;;
187.CREATE PROCEDURE spAssignmentsTranslated AS PARAMETERS TargetObjectTypeID Long, TargetObjectID Long, AssignedObjectTypeID Long, LocaleID Long; SELECT a2.ID, IIf(IsNull(t2.Value1),a2.DisplayName,t2.Value1) AS DisplayName FROM (SELECT s.DisplayName, s.ID, s.TranslationID FROM Assignments AS a INNER JOIN Segments AS s ON a.AssignedObjectID=s.ID WHERE a.TargetObjectTypeID = [TargetObjectTypeID] AND a.TargetObjectID = [TargetObjectID] AND iif([AssignedObjectTypeID]=0,a.AssignedObjectTypeID<>0, a.AssignedObjectTypeID = [AssignedObjectTypeID]))  AS a2 LEFT JOIN (SELECT ID, Value1, Value2, Value3, Value4 FROM Translations AS t WHERE t.[LocaleID]=[LocaleID])  AS t2 ON a2.TranslationID = t2.ID;;

188.DROP PROCEDURE spPeopleGroupsTranslated;;
189.CREATE PROCEDURE spPeopleGroupsTranslated AS PARAMETERS LocaleID Long; SELECT g.ID, IIf(t.Value1 Is Null,g.Name,t.Value1) AS Name, IIf(t.Value2 Is Null,g.Description,t.Value2) AS Description, g.Visible, g.CIGroup, g.LastEditTime FROM (SELECT * FROM  PeopleGroups)  AS g LEFT JOIN (SELECT ID, Value1, Value2 FROM Translations AS tr WHERE  tr.LocaleID=[LocaleID])  AS t ON g.TranslationID=t.ID ORDER BY g.Name;;

