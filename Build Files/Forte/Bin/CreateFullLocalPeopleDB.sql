DROP PROCEDURE spMetadataItemFromID;;
CREATE PROCEDURE spMetadataItemFromID as PARAMETERS Name Text ( 255 ); SELECT [Value] FROM Metadata AS m WHERE m.Name = [Name];;
DROP PROCEDURE spMetadataUpdate;;
CREATE PROCEDURE spMetaDataUpdate AS PARAMETERS Name Text ( 255 ), [Value] Text ( 255 ); UPDATE Metadata AS m SET m.[Value] = [Value] WHERE (((m.Name)=[Name]));;
DROP PROCEDURE spGetPeople;;
CREATE PROCEDURE spGetPeople AS SELECT p.ID1, p.UserID, p.FirstName, p.LastName, p.MI, p.DisplayName, p.ShortName, p.FullName, p.Initials, p.OfficeID, p.IsAttorney, p.IsAuthor, p.Phone, p.Fax, p.Email, p.AdmittedIn FROM People p WHERE p.ID2 = 0 AND p.LinkedPersonID = 0;;
DROP PROCEDURE spCreatePeopleTmp;;
CREATE PROCEDURE spCreatePeopleTmp AS SELECT 0 AS LinkedPersonID, 0 AS ID2, 1 AS UsageState, 0 AS OwnerID, 1 AS OfficeUsageState, Null AS DefaultOfficeRecordID1, -1 AS LinkedExternally, p.* INTO PeopleTmp FROM spGetPeople AS p;;
ALTER TABLE People ADD CONSTRAINT PK_PeopleID Primary Key (ID1, ID2);;