1.ALTER PROCEDURE [dbo].[spSyncUpPeopleAdd] 
AS	
	INSERT INTO People (ID1, ID2, OwnerID, UsageState, UserID, DisplayName, LinkedPersonID,Prefix,LastName,
		FirstName,MI,Suffix, FullName, ShortName, OfficeID, Initials, IsAttorney, IsAuthor, Title, 
		Phone, Fax, EMail, AdmittedIn, LastEditTime)
		SELECT s.ID1, s.ID2, s.OwnerID, s.UsageState, s.UserID, s.DisplayName, s.LinkedPersonID, s.Prefix, 
			s.LastName, s.FirstName, s.MI, s.Suffix, s.FullName, s.ShortName, s.OfficeID, s.Initials, 
			s.IsAttorney, s.IsAuthor, s.Title, s.Phone, s.Fax, s.EMail, s.AdmittedIn, s.LastEditTime 
			FROM SyncPeopleTmp s LEFT JOIN People p ON s.ID1 = p.ID1 AND s.ID2 = p.ID2 WHERE p.ID1 is null
	RETURN

GO
2.ALTER PROCEDURE [dbo].[spSyncUpPeopleUpdate] 
AS
	UPDATE People 
	SET People.OwnerID = SyncPeopleTmp.OwnerID, People.UsageState = SyncPeopleTmp.UsageState, People.UserID = SyncPeopleTmp.UserID, 
		People.DisplayName = SyncPeopleTmp.DisplayName, People.LinkedPersonID = SyncPeopleTmp.LinkedPersonID, People.Prefix = SyncPeopleTmp.Prefix, 
		People.LastName = SyncPeopleTmp.LastName, People.FirstName = SyncPeopleTmp.FirstName, People.MI = SyncPeopleTmp.MI, People.Suffix = SyncPeopleTmp.Suffix, 
		People.FullName = SyncPeopleTmp.FullName, People.ShortName = SyncPeopleTmp.ShortName, People.OfficeID = SyncPeopleTmp.OfficeID, 
		People.Initials = SyncPeopleTmp.Initials, People.IsAttorney = SyncPeopleTmp.IsAttorney, People.IsAuthor = SyncPeopleTmp.IsAuthor, 
		People.Title = SyncPeopleTmp.Title, People.Phone = SyncPeopleTmp.Phone, People.Fax = SyncPeopleTmp.Fax, People.EMail = SyncPeopleTmp.EMail, 
		People.AdmittedIn = SyncPeopleTmp.AdmittedIn, People.LastEditTime = SyncPeopleTmp.LastEditTime
	FROM PEOPLE INNER JOIN SyncPeopleTmp ON (People.ID1=SyncPeopleTmp.ID1) AND (People.ID2=SyncPeopleTmp.ID2)
	RETURN

GO
3.ALTER PROCEDURE [dbo].[spUserSyncDownGetAttyLicenses]
	(
		@UserID int,
		@LastSyncDown datetime
	)
AS
	SELECT l.* FROM AttyLicenses l INNER JOIN Proxies x ON l.OwnerID1 = x.UserID AND l.OwnerID2 = x.UserID2 
	WHERE (l.LastEditTime > @LastSyncDown OR x.LastEditTime > @LastSyncDown) AND x.ProxyID = @UserID  AND x.ProxyID2 = 0  
	
	UNION SELECT l.* FROM AttyLicenses l INNER JOIN [Permissions] m ON l.OwnerID1 = m.ObjectID1 AND l.OwnerID2 = 0 
	WHERE (l.LastEditTime > @LastSyncDown OR m.LastEditTime > @LastSyncDown) AND m.PersonID = @UserID
	
	UNION SELECT l.* FROM AttyLicenses l INNER JOIN [FavoritePeople] f ON l.OwnerID1 = f.PersonID1 AND l.OwnerID2 = 0 
	WHERE (l.LastEditTime > @LastSyncDown OR f.LastEditTime > @LastSyncDown) AND f.OwnerID1 = @UserID

	UNION SELECT l.* FROM AttyLicenses l INNER JOIN [People] p ON l.OwnerID1 = p.ID1 AND l.OwnerID2 = p.ID2
	WHERE (p.LastEditTime > @LastSyncDown OR l.LastEditTime > @LastSyncDown) AND((p.ID1= @UserID AND p.ID2 = 0) OR (p.OwnerID = @UserID OR p.LinkedPersonID = @UserID)) ;

	RETURN

GO
4.ALTER PROCEDURE [dbo].[spUserSyncDownGetPeople] 
	(
		@UserID int,
		@LastSyncDown datetime
	)
AS
	SELECT p.* FROM People p INNER JOIN Proxies x ON p.ID1 = x.UserID AND p.ID2 = x.UserID2 
	WHERE (p.LastEditTime > @LastSyncDown OR x.LastEditTime > @LastSyncDown) AND x.ProxyID = @UserID  AND x.ProxyID2 = 0  
	
	UNION SELECT p.* FROM People p INNER JOIN [Permissions] m ON p.ID1 = m.ObjectID1 AND p.ID2 = 0 
	WHERE (p.LastEditTime > @LastSyncDown OR m.LastEditTime > @LastSyncDown) AND m.PersonID = @UserID
	
	UNION SELECT p.* FROM People p INNER JOIN [FavoritePeople] f ON p.ID1 = f.PersonID1 AND p.ID2 = 0 
	WHERE (p.LastEditTime > @LastSyncDown OR f.LastEditTime > @LastSyncDown) AND f.OwnerID1 = @UserID

	UNION SELECT p.* FROM People p WHERE p.LastEditTime > @LastSyncDown AND((p.ID1= @UserID AND p.ID2 = 0) OR (p.OwnerID = @UserID OR p.LinkedPersonID = @UserID)) ;
	RETURN
	
GO
5.ALTER TABLE Keysets ADD Culture INT NOT NULL DEFAULT 1033;
GO
6.ALTER TABLE Keysets DROP CONSTRAINT [aaaaaKeysets_PK];
GO
7.ALTER TABLE Keysets ADD CONSTRAINT [aaaaaKeysets_PK] PRIMARY KEY CLUSTERED 
(
	[ScopeID1] ASC,
	[ScopeID2] ASC,
	[OwnerID1] ASC,
	[OwnerID2] ASC,
	[Type] ASC,
	[Culture] ASC
)
GO
8.UPDATE Keysets SET Culture=1033;
GO
9.ALTER TABLE SyncKeySetsTmp ADD Culture INT NOT NULL DEFAULT 1033;
GO
10.ALTER TABLE SyncKeySetsTmp DROP CONSTRAINT [PK_SyncKeySetsTmp];
GO
11.ALTER TABLE SyncKeySetsTmp ADD CONSTRAINT [PK_SyncKeySetsTmp] PRIMARY KEY CLUSTERED 
(
	[ScopeID1] ASC,
	[ScopeID2] ASC,
	[OwnerID1] ASC,
	[OwnerID2] ASC,
	[Type] ASC,
	[Culture] ASC
)
GO
12.ALTER PROCEDURE [dbo].[spAdminSyncDownGetKeySets] 
(
		@LastSyncDown datetime
)
AS
	SELECT ScopeID1, ScopeID2, OwnerID1, OwnerID2, [Type], Culture, [Values], UsageState,
		LastEditTime FROM KeySets WHERE LastEditTime > @LastSyncDown;
	RETURN
GO
13.ALTER PROCEDURE [dbo].[spAdminSyncUpKeySetsDelete] 
AS
	DELETE FROM KeySets
	WHERE  (ScopeID1 IN
                   (SELECT  t.ScopeID1
                    FROM     KeySets AS t LEFT OUTER JOIN
                                   SyncKeySetsTmp AS s ON t.ScopeID2 = s.ScopeID2 AND t.ScopeID1 = s.ScopeID1 AND t.OwnerID1 = s.OwnerID1 AND t.OwnerID2 = s.OwnerID2 AND 
                                   t.Type = s.Type AND t.Culture = s.Culture
                    WHERE  (t.ScopeID1 IS NOT NULL) AND (s.ScopeID2 IS NULL) AND (t.OwnerID1 IS NOT NULL) AND (s.OwnerID1 IS NULL) AND (t.OwnerID2 IS NOT NULL) AND (s.OwnerID2 IS NULL) 
                                   AND (t.Type IS NOT NULL) AND (s.Type IS NULL) AND (t.Culture IS NOT NULL) AND (s.Culture IS NULL))) AND (ScopeID2 IN
                   (SELECT  t.ScopeID2
                    FROM     KeySets AS t LEFT OUTER JOIN
                                   SyncKeySetsTmp AS s ON t.ScopeID2 = s.ScopeID2 AND t.ScopeID1 = s.ScopeID1 AND t.OwnerID1 = s.OwnerID1 AND t.OwnerID2 = s.OwnerID2 AND 
                                   t.Type = s.Type AND t.Culture = s.Culture
                    WHERE  (t.ScopeID1 IS NOT NULL) AND (s.ScopeID2 IS NULL) AND (t.OwnerID1 IS NOT NULL) AND (s.OwnerID1 IS NULL) AND (t.OwnerID2 IS NOT NULL) AND (s.OwnerID2 IS NULL) 
                                   AND (t.Type IS NOT NULL) AND (s.Type IS NULL) AND (t.Culture IS NOT NULL) AND (s.Culture IS NULL))) AND (OwnerID1 IN
                   (SELECT  t.OwnerID1
                    FROM     KeySets AS t LEFT OUTER JOIN
                                   SyncKeySetsTmp AS s ON t.ScopeID2 = s.ScopeID2 AND t.ScopeID1 = s.ScopeID1 AND t.OwnerID1 = s.OwnerID1 AND t.OwnerID2 = s.OwnerID2 AND 
                                   t.Type = s.Type AND t.Culture = s.Culture
                    WHERE  (t.ScopeID1 IS NOT NULL) AND (s.ScopeID2 IS NULL) AND (t.OwnerID1 IS NOT NULL) AND (s.OwnerID1 IS NULL) AND (t.OwnerID2 IS NOT NULL) AND (s.OwnerID2 IS NULL) 
                                   AND (t.Type IS NOT NULL) AND (s.Type IS NULL) AND (t.Culture IS NOT NULL) AND (s.Culture IS NULL))) AND (OwnerID2 IN
                   (SELECT  t.OwnerID2
                    FROM     KeySets AS t LEFT OUTER JOIN
                                   SyncKeySetsTmp AS s ON t.ScopeID2 = s.ScopeID2 AND t.ScopeID1 = s.ScopeID1 AND t.OwnerID1 = s.OwnerID1 AND t.OwnerID2 = s.OwnerID2 AND 
                                   t.Type = s.Type AND t.Culture = s.Culture
                    WHERE  (t.ScopeID1 IS NOT NULL) AND (s.ScopeID2 IS NULL) AND (t.OwnerID1 IS NOT NULL) AND (s.OwnerID1 IS NULL) AND (t.OwnerID2 IS NOT NULL) AND (s.OwnerID2 IS NULL) 
                                   AND (t.Type IS NOT NULL) AND (s.Type IS NULL) AND (t.Culture IS NOT NULL) AND (s.Culture IS NULL))) AND ([Type] IN
                   (SELECT  t.Type
                    FROM     KeySets AS t LEFT OUTER JOIN
                                   SyncKeySetsTmp AS s ON t.ScopeID2 = s.ScopeID2 AND t.ScopeID1 = s.ScopeID1 AND t.OwnerID1 = s.OwnerID1 AND t.OwnerID2 = s.OwnerID2 AND 
                                   t.Type = s.Type AND t.Culture = s.Culture
                    WHERE  (t.ScopeID1 IS NOT NULL) AND (s.ScopeID2 IS NULL) AND (t.OwnerID1 IS NOT NULL) AND (s.OwnerID1 IS NULL) AND (t.OwnerID2 IS NOT NULL) AND (s.OwnerID2 IS NULL) 
                                   AND (t.Type IS NOT NULL) AND (s.Type IS NULL) AND (t.Culture IS NOT NULL) AND (s.Culture IS NULL))) AND (Culture IN
                   (SELECT  t.Culture
                    FROM     KeySets AS t LEFT OUTER JOIN
                                   SyncKeySetsTmp AS s ON t.ScopeID2 = s.ScopeID2 AND t.ScopeID1 = s.ScopeID1 AND t.OwnerID1 = s.OwnerID1 AND t.OwnerID2 = s.OwnerID2 AND 
                                   t.Type = s.Type AND t.Culture = s.Culture
                    WHERE  (t.ScopeID1 IS NOT NULL) AND (s.ScopeID2 IS NULL) AND (t.OwnerID1 IS NOT NULL) AND (s.OwnerID1 IS NULL) AND (t.OwnerID2 IS NOT NULL) AND (s.OwnerID2 IS NULL) 
                                   AND (t.Type IS NOT NULL) AND (s.Type IS NULL) AND (t.Culture IS NOT NULL) AND (s.Culture IS NULL)));	
GO
14.ALTER PROCEDURE [dbo].[spSyncUpKeySetsAdd]
AS
	INSERT INTO KeySets
                      (ScopeID1, ScopeID2, OwnerID1, OwnerID2, [Type], Culture, UsageState, [Values], LastEditTime)
	SELECT  s.ScopeID1, s.ScopeID2, s.OwnerID1, s.OwnerID2, s.Type, s.Culture, s.UsageState, s.[Values], s.LastEditTime
	FROM     SyncKeySetsTmp AS s LEFT OUTER JOIN
               KeySets AS p ON s.ScopeID1 = p.ScopeID1 AND s.ScopeID2 = p.ScopeID2 AND s.OwnerID1 = p.OwnerID1 AND s.OwnerID2 = p.OwnerID2 AND s.Type = p.Type AND s.Culture = p.Culture
	WHERE  (p.ScopeID1 IS NULL) AND (p.ScopeID2 IS NULL) AND (p.OwnerID1 IS NULL) AND (p.OwnerID2 IS NULL) AND (p.Type IS NULL) AND (p.Culture IS NULL)
	RETURN
GO
15.ALTER PROCEDURE [dbo].[spSyncUpKeySetsUpdate]
AS
	UPDATE    KeySets
	SET              ScopeID1 = s.ScopeID1, ScopeID2 = s.ScopeID2, OwnerID1 = s.OwnerID1, OwnerID2 = s.OwnerID2, [Type] = s.Type, Culture = s.Culture,
						[Values] = s.[Values], UsageState = s.UsageState, LastEditTime = s.LastEditTime
	FROM         KeySets INNER JOIN
	                      SyncKeySetsTmp AS s ON KeySets.ScopeID1 = s.ScopeID1 AND KeySets.ScopeID2 = s.ScopeID2 AND KeySets.OwnerID1 = s.OwnerID1 AND 
	                      KeySets.OwnerID2 = s.OwnerID2 AND KeySets.Type = s.Type AND KeySets.Culture = s.Culture
	WHERE s.LastEditTime > KeySets.LastEditTime;
	RETURN
GO
16.ALTER PROCEDURE [dbo].[spUserSyncDownGetKeySets]
	(
		@UserID int,
		@LastSyncDown datetime
	)
AS
	SELECT ScopeID1, ScopeID2, OwnerID1, OwnerID2, [Type], Culture, [Values], UsageState, 
		LastEditTime FROM KeySets p 
	WHERE  p.LastEditTime > @LastSyncDown 
	AND
	(((p.OwnerID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) 
	OR (p.OwnerID1 = @UserID OR p.OwnerID1 = -999999999 OR p.OwnerID2 = -999999999 OR p.ScopeID2 = -999999999))
	OR (p.OwnerID1 = ANY(SELECT ID1 FROM People WHERE (OwnerID = @UserID OR LinkedPersonID = @UserID))) )
GO
17.ALTER PROCEDURE [dbo].[spUserSyncUpKeySetsDelete] 
	(
		@UserID int
	)
	AS
	DELETE FROM KeySets
	WHERE  (ScopeID1 IN
                   (SELECT  t.ScopeID1
                    FROM     KeySets AS t LEFT OUTER JOIN
                                   SyncKeySetsTmp AS s ON t.ScopeID2 = s.ScopeID2 AND t.ScopeID1 = s.ScopeID1 AND t.OwnerID1 = s.OwnerID1 AND t.OwnerID2 = s.OwnerID2 AND 
                                   t.Type = s.Type AND t.Culture = s.Culture
                    WHERE  (t.ScopeID1 IS NOT NULL) AND (s.ScopeID2 IS NOT NULL) AND (t.OwnerID1 IS NOT NULL) AND (s.OwnerID1 IS NOT NULL) AND (t.OwnerID2 IS NOT NULL) AND 
                                   (s.OwnerID2 IS NOT NULL) AND (t.Type IS NOT NULL) AND (s.Type IS NOT NULL) AND (t.Culture IS NOT NULL) AND (s.Culture IS NOT NULL))) AND (ScopeID2 =
                   (SELECT  t.ScopeID2
                    FROM     KeySets AS t LEFT OUTER JOIN
                                   SyncKeySetsTmp AS s ON t.ScopeID2 = s.ScopeID2 AND t.ScopeID1 = s.ScopeID1 AND t.OwnerID1 = s.OwnerID1 AND t.OwnerID2 = s.OwnerID2 AND 
                                   t.Type = s.Type AND t.Culture = s.Culture
                    WHERE  (t.ScopeID1 IS NOT NULL) AND (s.ScopeID2 IS NOT NULL) AND (t.OwnerID1 IS NOT NULL) AND (s.OwnerID1 IS NOT NULL) AND (t.OwnerID2 IS NOT NULL) AND 
                                   (s.OwnerID2 IS NOT NULL) AND (t.Type IS NOT NULL) AND (s.Type IS NOT NULL) AND (t.Culture IS NOT NULL) AND (s.Culture IS NOT NULL))) AND (OwnerID1 =
                   (SELECT  t.OwnerID1
                    FROM     KeySets AS t LEFT OUTER JOIN
                                   SyncKeySetsTmp AS s ON t.ScopeID2 = s.ScopeID2 AND t.ScopeID1 = s.ScopeID1 AND t.OwnerID1 = s.OwnerID1 AND t.OwnerID2 = s.OwnerID2 AND 
                                   t.Type = s.Type AND t.Culture = s.Culture
                    WHERE  (t.ScopeID1 IS NOT NULL) AND (s.ScopeID2 IS NOT NULL) AND (t.OwnerID1 IS NOT NULL) AND (s.OwnerID1 IS NOT NULL) AND (t.OwnerID2 IS NOT NULL) AND 
                                   (s.OwnerID2 IS NOT NULL) AND (t.Type IS NOT NULL) AND (s.Type IS NOT NULL) AND (t.Culture IS NOT NULL) AND (s.Culture IS NOT NULL))) AND (OwnerID2 =
                   (SELECT  t.OwnerID2
                    FROM     KeySets AS t LEFT OUTER JOIN
                                   SyncKeySetsTmp AS s ON t.ScopeID2 = s.ScopeID2 AND t.ScopeID1 = s.ScopeID1 AND t.OwnerID1 = s.OwnerID1 AND t.OwnerID2 = s.OwnerID2 AND 
                                   t.Type = s.Type AND t.Culture = s.Culture
                    WHERE  (t.ScopeID1 IS NOT NULL) AND (s.ScopeID2 IS NOT NULL) AND (t.OwnerID1 IS NOT NULL) AND (s.OwnerID1 IS NOT NULL) AND (t.OwnerID2 IS NOT NULL) AND 
                                   (s.OwnerID2 IS NOT NULL) AND (t.Type IS NOT NULL) AND (s.Type IS NOT NULL) AND (t.Culture IS NOT NULL) AND (s.Culture IS NOT NULL))) AND ([Type] =
                   (SELECT  t.Type
                    FROM     KeySets AS t LEFT OUTER JOIN
                                   SyncKeySetsTmp AS s ON t.ScopeID2 = s.ScopeID2 AND t.ScopeID1 = s.ScopeID1 AND t.OwnerID1 = s.OwnerID1 AND t.OwnerID2 = s.OwnerID2 AND 
                                   t.Type = s.Type AND t.Culture = s.Culture
                    WHERE  (t.ScopeID1 IS NOT NULL) AND (s.ScopeID2 IS NOT NULL) AND (t.OwnerID1 IS NOT NULL) AND (s.OwnerID1 IS NOT NULL) AND (t.OwnerID2 IS NOT NULL) AND 
                                   (s.OwnerID2 IS NOT NULL) AND (t.Type IS NOT NULL) AND (s.Type IS NOT NULL) AND (t.Culture IS NOT NULL) AND (s.Culture IS NOT NULL))) AND (Culture =
                   (SELECT  t.Culture
                    FROM     KeySets AS t LEFT OUTER JOIN
                                   SyncKeySetsTmp AS s ON t.ScopeID2 = s.ScopeID2 AND t.ScopeID1 = s.ScopeID1 AND t.OwnerID1 = s.OwnerID1 AND t.OwnerID2 = s.OwnerID2 AND 
                                   t.Type = s.Type AND t.Culture = s.Culture
                    WHERE  (t.ScopeID1 IS NOT NULL) AND (s.ScopeID2 IS NOT NULL) AND (t.OwnerID1 IS NOT NULL) AND (s.OwnerID1 IS NOT NULL) AND (t.OwnerID2 IS NOT NULL) AND 
                                   (s.OwnerID2 IS NOT NULL) AND (t.Type IS NOT NULL) AND (s.Type IS NOT NULL) AND (t.Culture IS NOT NULL) AND (s.Culture IS NOT NULL))
                                   
        AND OwnerID1 IN (SELECT UserID FROM Proxies WHERE ProxyID = @UserID) OR (OwnerID1 = @UserID)
			OR OwnerID1 IN (SELECT ID1 FROM People WHERE (OwnerID = @UserID OR LinkedPersonID = @UserID)));		
GO
18.CREATE PROCEDURE [dbo].[spAdminSyncDownGetUserSegments]
	(
		@LastSyncDown datetime
	)
	AS
		SELECT * FROM UserSegments WHERE LastEditTime > @LastSyncDown AND SharedFolderID > 0;
		RETURN
GO
19.ALTER TABLE [dbo].[Deletions] DROP CONSTRAINT [PK_Deletions];
GO
20.ALTER TABLE Deletions ALTER COLUMN ObjectTypeID int NOT NULL;
GO
21.ALTER TABLE Deletions ALTER COLUMN ObjectID nvarchar(255) NOT NULL;
GO
22.ALTER TABLE Deletions ADD CONSTRAINT [PK_Deletions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[ObjectTypeID] ASC,
	[ObjectID] ASC
)
GO
23.ALTER TABLE SyncDeletionsTmp ALTER COLUMN ObjectTypeID int NOT NULL;
GO
24.ALTER TABLE SyncDeletionsTmp ALTER COLUMN ObjectID nvarchar(255) NOT NULL;
GO
25.ALTER TABLE [dbo].[SyncDeletionsTmp] DROP CONSTRAINT [PK_SyncDeletionsTmp];
GO
26.ALTER TABLE SyncDeletionsTmp ADD CONSTRAINT [PK_SyncDeletionsTmp] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[ObjectTypeID] ASC,
	[ObjectID] ASC
)
GO
27.ALTER PROCEDURE [dbo].[spSyncUpDeletionsAdd] 
AS 
	SET IDENTITY_INSERT Deletions ON
	INSERT INTO Deletions (ID, ObjectTypeID, ObjectID, LastEditTime)
	SELECT  s.ID, s.ObjectTypeID, s.ObjectID, s.LastEditTime
	FROM     SyncDeletionsTmp AS s LEFT OUTER JOIN
					Deletions AS a ON (s.ID = a.ID AND s.ObjectTypeID = a.ObjectTypeID 
					AND s.ObjectID = a.ObjectID)
	WHERE  (a.ID IS NULL)
	RETURN
GO
28.ALTER PROCEDURE [dbo].[spSyncUpDeletionsUpdate] 
AS
	UPDATE Deletions
	SET LastEditTime = s.LastEditTime
	FROM  Deletions AS d INNER JOIN SyncDeletionsTmp AS s ON (d.ID = s.ID AND
		d.ObjectTypeID = s.ObjectTypeID AND d.ObjectID = s.ObjectID)
	WHERE s.LastEditTime > d.LastEditTime
	RETURN
GO
29.ALTER PROCEDURE [dbo].[spUserSyncDownGetPeople] 	
(		
	@UserID int,		
	@LastSyncDown datetime	
)
AS	
	SELECT p.* FROM People p 
	INNER JOIN Proxies x ON p.ID1 = x.UserID AND p.ID2 = x.UserID2 	WHERE 
	(p.LastEditTime > @LastSyncDown OR x.LastEditTime > @LastSyncDown) 
	AND x.ProxyID = @UserID  AND x.ProxyID2 = 0  		
	UNION SELECT p.* FROM People p INNER JOIN [Permissions] m ON p.ID1 = m.ObjectID1 AND p.ID2 = 0 	
	WHERE (p.LastEditTime > @LastSyncDown OR m.LastEditTime > @LastSyncDown) AND m.PersonID = @UserID
	UNION SELECT p.* FROM People p INNER JOIN [FavoritePeople] f ON p.ID1 = f.PersonID1 AND p.ID2 = 0 	
	WHERE (p.LastEditTime > @LastSyncDown OR f.LastEditTime > @LastSyncDown) AND f.OwnerID1 = @UserID	
	UNION SELECT p.* FROM People p WHERE p.LastEditTime > @LastSyncDown AND((p.ID1= @UserID AND p.ID2 = 0) 
	OR (p.OwnerID = @UserID OR p.LinkedPersonID = @UserID))
	UNION SELECT p.* FROM People p INNER JOIN [UserSegments] s ON p.ID1 = s.OwnerID1 AND p.ID2 = 0
	WHERE (p.LastEditTime > @LastSyncDown OR s.LastEditTime > @LastSyncDown) AND s.SharedFolderID > 0;
	RETURN
GO
30.ALTER PROCEDURE [dbo].[spUserSyncDownGetUserSegments] 
	(
		@UserID int,
		@LastSyncDown datetime
	)
AS
	SELECT     *
	FROM       UserSegments s
	WHERE    (s.LastEditTime > @LastSyncDown  
	AND
	((s.OwnerID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID))
	OR 
	(s.OwnerID1 = @UserID)
	
	OR (s.OwnerID1 = ANY (SELECT ID1 FROM People WHERE (OwnerID = @UserID OR LinkedPersonID = @UserID)))
	
	OR  
	
	(s.ID2 = ANY (SELECT ObjectID2 FROM Permissions WHERE PersonID = @UserID OR PersonID IN 
	(SELECT UserID FROM Proxies WHERE ProxyID = @UserID)))

	OR

	(SharedFolderID > 0)))

	OR

	(s.ID2 = ANY (SELECT ObjectID2 FROM Permissions p WHERE p.LastEditTime > @LastSyncDown AND
	(p.PersonID = @UserID OR p.PersonID IN 
	(SELECT UserID FROM Proxies WHERE ProxyID = @UserID))));
	
	RETURN
GO
31.ALTER PROCEDURE [dbo].[spPermissionsDeleteByObjectIDs] 
(
	@ObjectTypeID int, @ObjectID1 int, @ObjectID2 int, @PersonID int
)
	AS
	DELETE 
	FROM Permissions 
	WHERE ObjectTypeID = @ObjectTypeID AND ObjectID1 = @ObjectID1 AND ObjectID2 = @ObjectID2 
	AND (PersonID = @PersonID OR @PersonID is Null);
GO
32.ALTER PROCEDURE [dbo].[spUserSyncDownGetVariableSets] 
	(
		@UserID int,
		@LastSyncDown datetime
	)
	AS
	SELECT * FROM VariableSets v
	WHERE (v.LastEditTime > @LastSyncDown AND 
	((v.OwnerID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) 

	OR 
	(v.OwnerID1 = @UserID) 

	OR 
	(v.OwnerID1 = ANY (SELECT ID1 FROM People WHERE (OwnerID = @UserID OR LinkedPersonID = @UserID)))

	OR 
	(v.ID2 = ANY (SELECT ObjectID2 FROM Permissions WHERE PersonID = @UserID OR PersonID IN 
	(SELECT UserID FROM Proxies WHERE ProxyID = @UserID)))

	OR
	(v.SharedFolderID > 0)))

	OR
	(v.ID2 = ANY (SELECT ObjectID2 FROM Permissions p WHERE p.LastEditTime > @LastSyncDown AND
	(p.PersonID = @UserID OR p.PersonID IN 
	(SELECT UserID FROM Proxies WHERE ProxyID = @UserID))));
	RETURN
GO
33.ALTER PROCEDURE [dbo].[spUserSyncDownGetUserFolderMembers] 
	(
		@UserID int,
		@LastSyncDown datetime
	)
AS
	SELECT * FROM UserFolderMembers ufm INNER JOIN UserFolders uf ON ufm.UserFolderID1 = uf.ID1 AND
	ufm.UserFolderID2 = uf.ID2 
	WHERE ((ufm.LastEditTime > @LastSyncDown AND
	(((uf.OwnerID = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR uf.OwnerID = @UserID) 
	OR (uf.OwnerID = ANY (SELECT ID1 FROM PEOPLE WHERE OwnerID = @UserID OR LinkedPersonID = @UserID))))
	OR (ufm.TargetObjectID2 = ANY (SELECT ID2 FROM UserSegments s 
	WHERE (s.LastEditTime > @LastSyncDown AND (s.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR (s.ID1 = @UserID)
	OR (s.ID1 = ANY (SELECT ID1 FROM People WHERE (OwnerID = @UserID OR LinkedPersonID = @UserID))))))
	OR
	(ufm.TargetObjectID2 = ANY (SELECT ID2 FROM VariableSets v 
	WHERE (v.LastEditTime > @LastSyncDown AND (v.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR (v.ID1 = @UserID)
	OR (v.ID1 = ANY (SELECT ID1 FROM People WHERE (OwnerID = @UserID OR LinkedPersonID = @UserID)))))));
	RETURN
GO
34.ALTER PROCEDURE [dbo].[spUserSyncDownGetUserFolders] 
	(
		@UserID int,
		@LastSyncDown datetime
	)
AS
	SELECT * FROM UserFolders uf 
	WHERE ((LastEditTime > @LastSyncDown AND
	((uf.OwnerID = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR (uf.OwnerID = @UserID)
	OR (uf.OwnerID = ANY (SELECT ID1 FROM People WHERE (OwnerID = @UserID OR LinkedPersonID = @UserID)))))
	OR 
	(uf.ID2 = ANY (SELECT UserFolderID2 FROM UserFolderMembers fm 
	WHERE (fm.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID) OR (fm.ID1 = @UserID)
	OR (fm.ID1 = ANY (SELECT ID1 FROM People WHERE (OwnerID = @UserID OR LinkedPersonID = @UserID))
	AND 
	(fm.LastEditTime > @LastSyncDown OR (fm.TargetObjectID2 = ANY (SELECT ID2 FROM UserSegments s 
	WHERE (s.LastEditTime > @LastSyncDown AND (s.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR (s.ID1 = @UserID)
	OR (s.ID1 = ANY (SELECT ID1 FROM People WHERE (OwnerID = @UserID OR LinkedPersonID = @UserID))))))
	OR
	(fm.TargetObjectID2 = ANY (SELECT ID2 FROM VariableSets v 
	WHERE (v.LastEditTime > @LastSyncDown AND (v.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR (v.ID1 = @UserID)
	OR (v.ID1 = ANY (SELECT ID1 FROM People WHERE (OwnerID = @UserID OR LinkedPersonID = @UserID))))))))))));
	RETURN
GO
35.ALTER PROCEDURE [dbo].[spUserSyncDownGetPeople] 
	(
		@UserID int,
		@LastSyncDown datetime
	)
AS
	SELECT p.* FROM People p 
	INNER JOIN Proxies x ON p.ID1 = x.UserID AND p.ID2 = x.UserID2 	WHERE 
	(p.LastEditTime > @LastSyncDown OR x.LastEditTime > @LastSyncDown) 
	AND x.ProxyID = @UserID  AND x.ProxyID2 = 0  		
	UNION SELECT p.* FROM People p INNER JOIN [Permissions] m ON p.ID1 = m.ObjectID1 AND p.ID2 = 0 	
	WHERE (p.LastEditTime > @LastSyncDown OR m.LastEditTime > @LastSyncDown) AND m.PersonID = @UserID
	UNION SELECT p.* FROM People p INNER JOIN [FavoritePeople] f ON p.ID1 = f.PersonID1 AND p.ID2 = 0 	
	WHERE (p.LastEditTime > @LastSyncDown OR f.LastEditTime > @LastSyncDown) AND f.OwnerID1 = @UserID	
	UNION SELECT p.* FROM People p WHERE p.LastEditTime > @LastSyncDown AND((p.ID1= @UserID AND p.ID2 = 0) 
	OR (p.OwnerID = @UserID OR p.LinkedPersonID = @UserID))
	UNION SELECT p.* FROM People p INNER JOIN [UserSegments] s ON p.ID1 = s.OwnerID1 AND p.ID2 = 0
	WHERE (p.LastEditTime > @LastSyncDown OR s.LastEditTime > @LastSyncDown) AND s.SharedFolderID > 0
	UNION SELECT p.* FROM People p INNER JOIN [VariableSets] v ON p.ID1 = v.OwnerID1 AND p.ID2 = 0
	WHERE (p.LastEditTime > @LastSyncDown OR v.LastEditTime > @LastSyncDown) AND v.SharedFolderID > 0;
	RETURN
GO
36.ALTER PROCEDURE [dbo].[spUserSyncDownGetProxies] 
	(
		@UserID int,
		@LastSyncDown datetime
	)
AS
	SELECT * FROM Proxies p WHERE (p.ProxyID = @UserID OR p.UserID = @UserID) AND p.LastEditTime > @LastSyncDown;
	RETURN
GO
37.ALTER TABLE People ADD DefaultOfficeRecordID1 int NULL
GO
38.ALTER TABLE People ADD OfficeUsageState smallint NULL
GO
39.ALTER TABLE SyncPeopleTmp ADD DefaultOfficeRecordID1 int NULL
GO
40.ALTER TABLE SyncPeopleTmp ADD OfficeUsageState smallint NULL
GO
41.ALTER PROCEDURE [dbo].[spUserSyncDownGetPeople] 	
	(		
		@UserID int,		
		@LastSyncDown datetime	
	)
AS
	SELECT p.* FROM People p
	INNER JOIN Proxies x ON ((p.ID1 = x.UserID AND p.ID2 = x.UserID2) OR
	(p.ID1 = x.ProxyID AND p.ID2 = x.ProxyID2)) WHERE (p.LastEditTime > @LastSyncDown OR x.LastEditTime > @LastSyncDown) AND ((x.ProxyID = @UserID  AND 	x.ProxyID2 = 0) OR (x.UserID = @UserID AND
	x.UserID2 = 0))
	UNION SELECT p.* FROM People p INNER JOIN [Permissions] m ON p.ID1 =
	m.ObjectID1 AND p.ID2 = 0 	
	WHERE (p.LastEditTime > @LastSyncDown OR m.LastEditTime > @LastSyncDown) AND m.PersonID = @UserID UNION SELECT p.* FROM People p INNER JOIN 	[FavoritePeople] f ON p.ID1 =
	f.PersonID1 AND p.ID2 = 0 	
	WHERE (p.LastEditTime > @LastSyncDown OR f.LastEditTime > @LastSyncDown) AND
	f.OwnerID1 = @UserID	
	UNION SELECT p.* FROM People p WHERE p.LastEditTime > @LastSyncDown AND((p.ID1= @UserID AND p.ID2 = 0) OR (p.OwnerID = @UserID OR p.LinkedPersonID = 	@UserID)) UNION SELECT p.* FROM People p INNER JOIN [UserSegments] s ON p.ID1 =
	s.OwnerID1 AND p.ID2 = 0
	WHERE (p.LastEditTime > @LastSyncDown OR s.LastEditTime > @LastSyncDown) AND s.SharedFolderID > 0 UNION SELECT p.* FROM People p INNER JOIN 	[VariableSets] v ON p.ID1 =
	v.OwnerID1 AND p.ID2 = 0
	WHERE (p.LastEditTime > @LastSyncDown OR v.LastEditTime > @LastSyncDown) AND v.SharedFolderID > 0; 
	RETURN
GO
42.ALTER TABLE People ADD LinkedExternally bit
GO
43.ALTER PROCEDURE [dbo].[spUserSyncDownGetPeople] 	
	(		
		@UserID int,		
		@LastSyncDown datetime	
	)
AS
	SELECT p.* FROM People p
	INNER JOIN Proxies x ON ((p.ID1 = x.UserID AND p.ID2 = x.UserID2) OR
	(p.ID1 = x.ProxyID AND p.ID2 = x.ProxyID2)) WHERE (p.LastEditTime > @LastSyncDown OR x.LastEditTime > @LastSyncDown) AND 
	((x.ProxyID = @UserID  AND x.ProxyID2 = 0) OR  (x.UserID = @UserID AND x.UserID2 = 0))
	UNION SELECT p.* FROM People p INNER JOIN [Permissions] m ON p.ID1 = m.ObjectID1 AND p.ID2 = 0 
	WHERE (p.LastEditTime > @LastSyncDown OR m.LastEditTime > @LastSyncDown) 
	AND m.PersonID = @UserID 
	UNION SELECT p.* FROM People p INNER JOIN [FavoritePeople] f ON p.ID1 =
	f.PersonID1 AND p.ID2 = 0 	
	WHERE (p.LastEditTime > @LastSyncDown OR f.LastEditTime > @LastSyncDown) AND
	(f.OwnerID1 = @UserID OR (f.OwnerID1 IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID)))
	UNION SELECT p.* FROM People p WHERE p.LastEditTime > @LastSyncDown AND ((p.ID1= @UserID OR (p.ID1 IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID)) 
	AND p.ID2 = 0) OR (p.OwnerID = @UserID OR (p.OwnerID IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID)) 
	OR p.LinkedPersonID = @UserID OR (p.LinkedPersonID IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID))))
	UNION SELECT p.* FROM People p INNER JOIN [UserSegments] s ON p.ID1 = s.OwnerID1 AND p.ID2 = 0
	WHERE (p.LastEditTime > @LastSyncDown OR s.LastEditTime > @LastSyncDown) AND s.SharedFolderID > 0 
	UNION SELECT p.* FROM People p INNER JOIN [VariableSets] v ON p.ID1 =
	v.OwnerID1 AND p.ID2 = 0 
	WHERE (p.LastEditTime > @LastSyncDown OR v.LastEditTime > @LastSyncDown) AND v.SharedFolderID > 0;
	RETURN
GO
44.ALTER PROCEDURE [dbo].[spGetUserNames] 
AS
	SELECT DisplayName, ID1 FROM People WHERE UserID Is Not Null  And UserID <> '' and LinkedPersonID = 0 And UsageState=1 ORDER BY DisplayName
	RETURN
GO
45.CREATE PROCEDURE [dbo].[spAttyLicensesDelete]
(
	@ID1 int, @ID2 int
)
AS
	DELETE 
	FROM AttyLicenses WHERE AttyLicenses.ID1 = @ID1 AND AttyLicenses.ID2 = @ID2;
GO
46.ALTER PROCEDURE [dbo].[spUserSyncDownGetUserFolderMembers] 
	(
		@UserID int,
		@LastSyncDown datetime
	)
AS
	SELECT * FROM UserFolderMembers ufm INNER JOIN UserFolders uf ON ufm.UserFolderID1 = uf.ID1 AND
	ufm.UserFolderID2 = uf.ID2 
	WHERE ((((uf.OwnerID = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR uf.OwnerID = @UserID) 
	OR (uf.OwnerID = ANY (SELECT ID1 FROM PEOPLE WHERE OwnerID = @UserID OR LinkedPersonID = @UserID))) AND 
	(ufm.LastEditTime > @LastSyncDown OR (ufm.TargetObjectID2 = ANY (SELECT ID2 FROM UserSegments s 
	WHERE (s.LastEditTime > @LastSyncDown AND (s.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR (s.ID1 = @UserID)
	OR (s.ID1 = ANY (SELECT ID1 FROM People WHERE (OwnerID = @UserID OR LinkedPersonID = @UserID))))))
	OR (ufm.TargetObjectID2 = ANY (SELECT ID2 FROM VariableSets v 
	WHERE (v.LastEditTime > @LastSyncDown AND (v.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR (v.ID1 = @UserID)
	OR (v.ID1 = ANY (SELECT ID1 FROM People WHERE (OwnerID = @UserID OR LinkedPersonID = @UserID))))))));
	RETURN
GO
47.ALTER PROCEDURE [dbo].[spUserSyncDownGetUserFolders] 
	(
		@UserID int,
		@LastSyncDown datetime
	)
AS
	SELECT * FROM UserFolders uf 
	WHERE ((LastEditTime > @LastSyncDown AND
	((uf.OwnerID = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR (uf.OwnerID = @UserID)))
	OR 
	(uf.ID2 = ANY (SELECT UserFolderID2 FROM UserFolderMembers fm 
	WHERE (fm.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID) OR (fm.ID1 = @UserID)
	AND (fm.LastEditTime > @LastSyncDown OR (fm.TargetObjectID2 = ANY (SELECT ID2 FROM UserSegments s 
	WHERE (s.LastEditTime > @LastSyncDown AND (s.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR (s.ID1 = @UserID))))
	OR
	(fm.TargetObjectID2 = ANY (SELECT ID2 FROM VariableSets v 
	WHERE (v.LastEditTime > @LastSyncDown AND (v.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR (v.ID1 = @UserID)))))))));
	RETURN
GO
48.ALTER PROCEDURE [dbo].[spUserSyncDownGetUserFolderMembers] 
	(
		@UserID int,
		@LastSyncDown datetime
	)
AS
	SELECT * FROM UserFolderMembers ufm INNER JOIN UserFolders uf ON ufm.UserFolderID1 = uf.ID1 AND
	ufm.UserFolderID2 = uf.ID2 
	WHERE ((((uf.OwnerID = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR uf.OwnerID = @UserID)) AND 
	(ufm.LastEditTime > @LastSyncDown OR (ufm.TargetObjectID2 = ANY (SELECT ID2 FROM UserSegments s 
	WHERE (s.LastEditTime > @LastSyncDown AND (s.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR (s.ID1 = @UserID))))
	OR (ufm.TargetObjectID2 = ANY (SELECT ID2 FROM VariableSets v 
	WHERE (v.LastEditTime > @LastSyncDown AND (v.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR (v.ID1 = @UserID))))));
	RETURN
GO
49.ALTER PROCEDURE [dbo].[spUserSyncDownGetUserSegments] 
	(
		@UserID int,
		@LastSyncDown datetime
	)
AS
	SELECT     *
	FROM       UserSegments s
	WHERE    (s.LastEditTime > @LastSyncDown  
	AND
	((s.OwnerID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID))
	OR 
	(s.OwnerID1 = @UserID)
	OR  
	(s.ID2 = ANY (SELECT ObjectID2 FROM Permissions WHERE PersonID = @UserID OR PersonID IN 
	(SELECT UserID FROM Proxies WHERE ProxyID = @UserID)))
	OR
	(SharedFolderID > 0)))
	OR
	(s.ID2 = ANY (SELECT ObjectID2 FROM Permissions p WHERE p.LastEditTime > @LastSyncDown AND
	(p.PersonID = @UserID OR p.PersonID IN 
	(SELECT UserID FROM Proxies WHERE ProxyID = @UserID))));
	RETURN
GO
50.ALTER PROCEDURE [dbo].[spUserSyncDownGetVariableSets] 
	(
		@UserID int,
		@LastSyncDown datetime
	)
	AS
	SELECT * FROM VariableSets v
	WHERE (v.LastEditTime > @LastSyncDown AND 
	((v.OwnerID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) 
	OR 
	(v.OwnerID1 = @UserID) 
	OR 
	(v.ID2 = ANY (SELECT ObjectID2 FROM Permissions WHERE PersonID = @UserID OR PersonID IN 
	(SELECT UserID FROM Proxies WHERE ProxyID = @UserID)))
	OR
	(v.SharedFolderID > 0)))
	OR
	(v.ID2 = ANY (SELECT ObjectID2 FROM Permissions p WHERE p.LastEditTime > @LastSyncDown AND
	(p.PersonID = @UserID OR p.PersonID IN 
	(SELECT UserID FROM Proxies WHERE ProxyID = @UserID))));
	RETURN
GO
51.ALTER PROCEDURE [dbo].[spUserSyncDownGetPeople] 	
	(		
		@UserID int,		
		@LastSyncDown datetime	
	)
	AS
	SELECT p.* FROM People p
	INNER JOIN Proxies x ON ((p.ID1 = x.UserID AND p.ID2 = x.UserID2) OR
	(p.ID1 = x.ProxyID AND p.ID2 = x.ProxyID2)) WHERE (p.LastEditTime > @LastSyncDown OR x.LastEditTime > @LastSyncDown) AND 
	((x.ProxyID = @UserID  AND x.ProxyID2 = 0) OR  (x.UserID = @UserID AND x.UserID2 = 0))
	UNION SELECT p.* FROM People p INNER JOIN [Permissions] m ON p.ID1 = m.ObjectID1 AND p.ID2 = 0 	
	WHERE (p.LastEditTime > @LastSyncDown OR m.LastEditTime > @LastSyncDown) 
	AND (m.PersonID = @UserID OR (m.PersonID IN (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)))
	UNION SELECT p.* FROM People p INNER JOIN [FavoritePeople] f ON p.ID1 = f.PersonID1 AND p.ID2 = 0 	
	WHERE (p.LastEditTime > @LastSyncDown OR f.LastEditTime > @LastSyncDown) AND
	(f.OwnerID1 = @UserID OR (f.OwnerID1 IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID)))
	UNION SELECT p.* FROM People p WHERE p.LastEditTime > @LastSyncDown AND 
	((p.ID1= @UserID OR (p.ID1 IN (SELECT UserID FROM Proxies WHERE 	ProxyID=@UserID)) 
	AND p.ID2 = 0) OR (p.OwnerID = @UserID OR (p.OwnerID IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID)) 
	OR p.LinkedPersonID = @UserID OR (p.LinkedPersonID IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID))))
	UNION SELECT p.* FROM People p INNER JOIN [UserSegments] s ON p.ID1 = s.OwnerID1 AND p.ID2 = 0
	WHERE (p.LastEditTime > @LastSyncDown OR s.LastEditTime > @LastSyncDown) AND s.SharedFolderID > 0 
	UNION SELECT p.* FROM People p INNER JOIN [VariableSets] v ON p.ID1 = v.OwnerID1 AND p.ID2 = 0 
	WHERE (p.LastEditTime > @LastSyncDown OR v.LastEditTime > @LastSyncDown) AND v.SharedFolderID > 0;
	RETURN
GO
52.ALTER PROCEDURE [dbo].[spUserSyncDownGetPeople] 	
	(		
		@UserID int,		
		@LastSyncDown datetime	
	)
AS
	SELECT p.* FROM People p
	INNER JOIN Proxies x ON ((p.ID1 = x.UserID AND p.ID2 = x.UserID2) OR
	(p.ID1 = x.ProxyID AND p.ID2 = x.ProxyID2)) WHERE (p.LastEditTime > @LastSyncDown OR x.LastEditTime > @LastSyncDown) AND 
	((x.ProxyID = @UserID  AND x.ProxyID2 = 0) OR  (x.UserID = @UserID AND x.UserID2 = 0))
	UNION SELECT p.* FROM People p INNER JOIN [Permissions] m ON p.ID1 = m.ObjectID1 AND p.ID2 = 0 
	WHERE (p.LastEditTime > @LastSyncDown OR m.LastEditTime > @LastSyncDown) 
	AND m.PersonID = @UserID 
	UNION SELECT p.* FROM People p INNER JOIN [FavoritePeople] f ON (p.ID1 = 
	f.PersonID1 AND p.ID2 = 0) OR (p.ID2=0 AND p.LinkedPersonID=f.PersonID1) 
	WHERE (p.LastEditTime > @LastSyncDown OR f.LastEditTime > @LastSyncDown) AND
	(f.OwnerID1 = @UserID OR (f.OwnerID1 IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID)))
	UNION SELECT p.* FROM People p WHERE p.LastEditTime > @LastSyncDown AND ((p.ID1= @UserID OR (p.ID1 IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID)) 
	AND p.ID2 = 0) OR (p.OwnerID = @UserID OR (p.OwnerID IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID)) 
	OR p.LinkedPersonID = @UserID OR (p.LinkedPersonID IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID))))
	UNION SELECT p.* FROM People p INNER JOIN [UserSegments] s ON p.ID1 = s.OwnerID1 AND p.ID2 = 0
	WHERE (p.LastEditTime > @LastSyncDown OR s.LastEditTime > @LastSyncDown) AND s.SharedFolderID > 0 
	UNION SELECT p.* FROM People p INNER JOIN [VariableSets] v ON p.ID1 =
	v.OwnerID1 AND p.ID2 = 0 
	WHERE (p.LastEditTime > @LastSyncDown OR v.LastEditTime > @LastSyncDown) AND v.SharedFolderID > 0;
	RETURN
GO
53.ALTER PROCEDURE [dbo].[spSyncUpPeopleAdd] 
AS	
	INSERT INTO People (ID1, ID2, OwnerID, UsageState, UserID, DisplayName, 
	LinkedPersonID, Prefix, LastName, FirstName, MI, Suffix, FullName, ShortName, OfficeID, 
	Initials, IsAttorney, IsAuthor, Title, Phone, Fax, EMail, AdmittedIn, 
	DefaultOfficeRecordID1, OfficeUsageState, LastEditTime) 
	SELECT s.ID1, s.ID2, s.OwnerID, s.UsageState, s.UserID, s.DisplayName, 
	s.LinkedPersonID, s.Prefix, s.LastName, s.FirstName, s.MI, s.Suffix, s.FullName, s.ShortName, 
	s.OfficeID, s.Initials, s.IsAttorney, s.IsAuthor, s.Title, s.Phone, s.Fax, s.EMail, s.AdmittedIn, 
	s.DefaultOfficeRecordID1, s.OfficeUsageState, s.LastEditTime 
	FROM SyncPeopleTmp s LEFT JOIN People p ON s.ID1 = p.ID1 AND s.ID2 = p.ID2 WHERE p.ID1 is null;
	RETURN

GO
54.ALTER PROCEDURE [dbo].[spSyncUpPeopleUpdate] 
AS
	UPDATE People SET OwnerID = s.OwnerID, UsageState = s.UsageState, UserID = s.UserID, 
	DisplayName = s.DisplayName, LinkedPersonID = s.LinkedPersonID, Prefix = s.Prefix, 
	LastName = s.LastName, FirstName = s.FirstName, MI = s.MI, Suffix = s.Suffix, 
	FullName = s.FullName, ShortName = s.ShortName, OfficeID = s.OfficeID, 
	Initials = s.Initials, IsAttorney = s.IsAttorney, IsAuthor = s.IsAuthor, 
	Title = s.Title, Phone = s.Phone, Fax = s.Fax, EMail = s.EMail, AdmittedIn = s.AdmittedIn, 
	DefaultOfficeRecordID1=s.DefaultOfficeRecordID1, OfficeUsageState=s.OfficeUsageState, LastEditTime = s.LastEditTime 
	FROM PEOPLE p INNER JOIN SyncPeopleTmp s ON (p.ID1=s.ID1) AND (p.ID2=s.ID2)
	RETURN

GO
55.ALTER PROCEDURE [dbo].[spUserSyncDownGetPeople] 
	(		
		@UserID int,		
		@LastSyncDown datetime	
	)
AS	
	SELECT p.* FROM People p 
	INNER JOIN Proxies x ON ((p.ID1 = x.UserID AND p.ID2 = x.UserID2) OR 
	(p.ID1 = x.ProxyID AND p.ID2 = x.ProxyID2)) WHERE (p.LastEditTime > @LastSyncDown OR x.LastEditTime > @LastSyncDown) AND 
	((x.ProxyID = @UserID  AND x.ProxyID2 = 0) OR  (x.UserID = @UserID AND x.UserID2 = 0)) 
	UNION SELECT p.* FROM People p INNER JOIN [Permissions] m ON p.ID1 = m.ObjectID1 AND p.ID2 = 0 
	WHERE (p.LastEditTime > @LastSyncDown OR m.LastEditTime > @LastSyncDown) 
	AND m.PersonID = @UserID 
	UNION SELECT p.* FROM People p INNER JOIN [FavoritePeople] f ON (p.ID1 = 
	f.PersonID1 AND p.ID2 = 0) OR (p.ID2=0 AND p.LinkedPersonID=f.PersonID1) 
	WHERE (p.LastEditTime > @LastSyncDown OR f.LastEditTime > @LastSyncDown) AND 
	(f.OwnerID1 = @UserID OR (f.OwnerID1 IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID))) 
	UNION SELECT p.* FROM People p WHERE p.LastEditTime > @LastSyncDown AND ((p.ID1= @UserID OR (p.ID1 IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID)) 
	AND p.ID2 = 0) OR (p.OwnerID = @UserID OR (p.OwnerID IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID)) 
	OR p.LinkedPersonID = @UserID OR (p.LinkedPersonID IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID)))) 
	UNION SELECT p.* FROM People p INNER JOIN [UserSegments] s ON p.ID1 = s.OwnerID1 AND p.ID2 = 0 
	WHERE (p.LastEditTime > @LastSyncDown OR s.LastEditTime > @LastSyncDown) AND s.SharedFolderID > 0 
	UNION SELECT p.* FROM People p INNER JOIN [VariableSets] v ON p.ID1 =
	v.OwnerID1 AND p.ID2 = 0 
	WHERE (p.LastEditTime > @LastSyncDown OR v.LastEditTime > @LastSyncDown) AND v.SharedFolderID > 0;
	RETURN
GO
56.ALTER PROCEDURE [dbo].[spUserSyncDownGetPeople] 
	(		
		@UserID int,		
		@LastSyncDown datetime	
	)
AS
	SELECT p.* FROM People p 
	INNER JOIN Proxies x ON ((p.ID1 = x.UserID AND p.ID2 = x.UserID2) OR 
	(p.ID1 = x.ProxyID AND p.ID2 = x.ProxyID2)) WHERE (p.LastEditTime > @LastSyncDown OR x.LastEditTime > @LastSyncDown) AND 
	((x.ProxyID = @UserID  AND x.ProxyID2 = 0) OR  (x.UserID = @UserID AND x.UserID2 = 0)) 
	UNION SELECT p.* FROM People p INNER JOIN [Permissions] m ON p.ID1 =
	m.ObjectID1 AND p.ID2 = 0 
	WHERE (p.LastEditTime > @LastSyncDown OR m.LastEditTime > @LastSyncDown) 
	AND (m.PersonID = @UserID OR (m.PersonID IN (Select UserID from Proxies WHERE ProxyID=@UserID))) 
	UNION SELECT p.* FROM People p INNER JOIN [FavoritePeople] f ON p.ID1 =
	f.PersonID1 AND p.ID2 = 0 
	WHERE (p.LastEditTime > @LastSyncDown OR f.LastEditTime > @LastSyncDown) AND 
	(f.OwnerID1 = @UserID OR (f.OwnerID1 IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID))) 
	UNION SELECT p.* FROM People p WHERE p.LastEditTime > @LastSyncDown AND ((p.ID1= @UserID OR (p.ID1 IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID)) 
	AND p.ID2 = 0) OR (p.OwnerID = @UserID OR (p.OwnerID IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID)) 
	OR p.LinkedPersonID = @UserID OR (p.LinkedPersonID IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID)))) 
	UNION SELECT p.* FROM People p INNER JOIN [UserSegments] s ON p.ID1 = s.OwnerID1 AND p.ID2 = 0 
	WHERE (p.LastEditTime > @LastSyncDown OR s.LastEditTime > @LastSyncDown) AND s.SharedFolderID > 0 
	UNION SELECT p.* FROM People p INNER JOIN [VariableSets] v ON p.ID1 =
	v.OwnerID1 AND p.ID2 = 0 
	WHERE (p.LastEditTime > @LastSyncDown OR v.LastEditTime > @LastSyncDown) AND v.SharedFolderID > 0;
RETURN
GO
57.ALTER PROCEDURE [dbo].[spUserSyncDownGetKeySets]
	(
		@UserID int,
		@LastSyncDown datetime
	)
AS
	SELECT ScopeID1, ScopeID2, OwnerID1, OwnerID2, [Type], Culture, [Values], UsageState, 
		LastEditTime FROM KeySets p 
	WHERE  p.LastEditTime > @LastSyncDown 
	AND
	(((p.OwnerID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) 
OR (p.OwnerID1 = @UserID OR p.OwnerID1 = -999999999 OR p.OwnerID2 = -999999999))
OR (p.OwnerID1 = ANY(SELECT ID1 FROM People WHERE (OwnerID = @UserID OR LinkedPersonID = @UserID))) )
GO
58.INSERT INTO MetaData ([Name], [Value]) SELECT 'ServerGUID', NewID();
GO
59.ALTER PROCEDURE [dbo].[spUserSyncDownGetKeySets]	
	(	@UserID int,		
		@LastSyncDown datetime	
	)
AS	
	SELECT ScopeID1, ScopeID2, OwnerID1, OwnerID2, [Type], Culture, [Values], UsageState, LastEditTime FROM KeySets p 	
	WHERE  p.LastEditTime > @LastSyncDown AND
	((p.OwnerID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR 
	(p.OwnerID1 = @UserID OR p.OwnerID1 = -999999999 OR p.OwnerID2 = -999999999) OR 
	(p.OwnerID1 = ANY (SELECT ID1 FROM People WHERE (OwnerID = @UserID OR LinkedPersonID = @UserID))) OR
	(p.OwnerID1 = ANY (SELECT PersonID1 FROM FavoritePeople WHERE (OwnerID1=@UserID))) OR
	(p.OwnerID1 = ANY (SELECT ID1 FROM People WHERE (ID2=0 AND 
	LinkedPersonID=ANY (SELECT PersonID1 FROM FavoritePeople WHERE (OwnerID1=@UserID))))))
GO
60.ALTER PROCEDURE [dbo].[spUserSyncDownGetUserFolders] 
	(
		@UserID int,
		@LastSyncDown datetime
	)
AS
	SELECT * FROM UserFolders uf 
	WHERE ((LastEditTime > @LastSyncDown AND
	((uf.OwnerID = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR (uf.OwnerID = @UserID))) OR 
	((uf.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID) OR (uf.ID1 = @UserID)) AND uf.ID2 = 
	ANY (SELECT UserFolderID2 FROM UserFolderMembers fm WHERE ((fm.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID) 
	OR (fm.ID1 = @UserID)) AND (fm.LastEditTime > @LastSyncDown OR (fm.TargetObjectID2 = ANY (SELECT ID2 FROM UserSegments s 
	WHERE (s.LastEditTime > @LastSyncDown AND (s.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR (s.ID1 = @UserID))))
	OR (fm.TargetObjectID2 = ANY (SELECT ID2 FROM VariableSets v 
	WHERE (v.LastEditTime > @LastSyncDown AND (v.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR (v.ID1 = @UserID)))))))));
GO
61.ALTER TABLE Offices ALTER COLUMN CustomField1 nvarchar(255) NULL;
GO
62.ALTER TABLE SyncOfficesTmp ALTER COLUMN CustomField1 nvarchar(255) NULL;
GO
63.ALTER PROCEDURE [dbo].[spUserSyncDownGetPeople] 
	(		
		@UserID int,		
		@LastSyncDown datetime	
	)
AS
	SELECT p.* FROM People p 
	INNER JOIN Proxies x ON ((p.ID1 = x.UserID AND p.ID2 = x.UserID2) OR 
	(p.ID1 = x.ProxyID AND p.ID2 = x.ProxyID2)) WHERE (p.LastEditTime > @LastSyncDown OR x.LastEditTime > @LastSyncDown) AND 
	((x.ProxyID = @UserID  AND x.ProxyID2 = 0) OR  (x.UserID = @UserID AND x.UserID2 = 0)) 
	UNION SELECT p.* FROM People p INNER JOIN [Permissions] m ON p.ID1 =
	m.ObjectID1 AND p.ID2 = 0 
	WHERE (p.LastEditTime > @LastSyncDown OR m.LastEditTime > @LastSyncDown) 
	AND (m.PersonID = @UserID OR (m.PersonID IN (Select UserID from Proxies WHERE ProxyID=@UserID))) 
	UNION SELECT p.* FROM People p INNER JOIN [FavoritePeople] f ON p.ID1 =
	f.PersonID1 AND p.ID2 = 0 
	WHERE (p.LastEditTime > @LastSyncDown OR f.LastEditTime > @LastSyncDown) AND 
	(f.OwnerID1 = @UserID OR (f.OwnerID1 IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID))) 
	UNION SELECT p.* FROM People p WHERE p.LastEditTime > @LastSyncDown AND ((p.ID1= @UserID OR (p.ID1 IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID)) 
	AND p.ID2 = 0) OR (p.OwnerID = @UserID OR (p.OwnerID IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID)) 
	OR p.LinkedPersonID = @UserID OR (p.LinkedPersonID IN (SELECT UserID FROM Proxies WHERE ProxyID=@UserID)) OR (p.LinkedPersonID IN (SELECT PersonID1 FROM FavoritePeople WHERE OwnerID1=@UserID)))) 
	UNION SELECT p.* FROM People p INNER JOIN [UserSegments] s ON p.ID1 = s.OwnerID1 AND p.ID2 = 0 
	WHERE (p.LastEditTime > @LastSyncDown OR s.LastEditTime > @LastSyncDown) AND s.SharedFolderID > 0 
	UNION SELECT p.* FROM People p INNER JOIN [VariableSets] v ON p.ID1 =
	v.OwnerID1 AND p.ID2 = 0 
	WHERE (p.LastEditTime > @LastSyncDown OR v.LastEditTime > @LastSyncDown) AND v.SharedFolderID > 0;
RETURN
GO
64.CREATE PROCEDURE [dbo].[spKeysetDelete] 
(
	@ScopeID1 int, 
	@ScopeID2 int, 
	@OwnerID1 int, 
	@OwnerID2 int, 
	@Type smallint, 
	@Culture int
) 
AS 
	DELETE FROM KeySets 
	WHERE ScopeID1=@ScopeID1 And ScopeID2=@ScopeID2 
	And OwnerID1=@OwnerID1 AND OwnerID2=@OwnerID2
	And [Type]=@Type And Culture=@Culture;
GO
65.ALTER PROCEDURE [dbo].[spSyncUpAttyLicensesAdd]
	AS
	INSERT INTO AttyLicenses (ID1, ID2, OwnerID1, OwnerID2, [Description], License, L0, L1, L2, L3, L4, 
		UsageState, LastEditTime)
		SELECT  s.ID1, s.ID2, s.OwnerID1, s.OwnerID2, s.[Description], s.License, s.L0, s.L1, s.L2, s.L3, s.L4, s.UsageState, s.LastEditTime
		FROM     SyncAttyLicensesTmp AS s LEFT OUTER JOIN
		               AttyLicenses AS p ON s.ID1 = p.ID1 AND s.ID2 = p.ID2
		WHERE  (p.ID1 IS NULL) AND (s.OwnerID1 IN (SELECT  ID1 FROM     People))
RETURN
GO
66.IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Permissions_People]') AND parent_object_id = OBJECT_ID(N'[dbo].[Permissions]'))
	ALTER TABLE [dbo].[Permissions] DROP CONSTRAINT [FK_Permissions_People]
GO
67.ALTER PROCEDURE [dbo].[spUserSyncDownGetUserFolders] 
	(
		@UserID int,
		@LastSyncDown datetime
	)
AS
	SELECT * FROM UserFolders uf 
	WHERE ((LastEditTime > @LastSyncDown AND
	((uf.OwnerID = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR (uf.OwnerID = @UserID))) OR 
	((uf.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID) OR (uf.ID1 = @UserID)) AND uf.ID2 = 
	ANY (SELECT UserFolderID2 FROM UserFolderMembers fm WHERE ((fm.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID) 
	OR (fm.ID1 = @UserID)) AND (fm.LastEditTime > @LastSyncDown OR (fm.TargetObjectID2 = ANY (SELECT ID2 FROM UserSegments s 
	WHERE (s.LastEditTime > @LastSyncDown AND (s.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR (s.ID1 = @UserID))))
	OR (fm.TargetObjectID2 = ANY (SELECT ID2 FROM VariableSets v 
	WHERE (v.LastEditTime > @LastSyncDown AND (v.ID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) OR (v.ID1 = @UserID)))))))));
GO
68.ALTER PROCEDURE [dbo].[spUserSyncDownGetUserSegments] 
	(
		@UserID int,
		@LastSyncDown datetime
	)
AS
	SELECT     *
	FROM       UserSegments s
	WHERE    (s.LastEditTime > @LastSyncDown  
	AND
	((s.OwnerID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID))
	OR 
	(s.OwnerID1 = @UserID)
	OR  
	(s.ID2 = ANY (SELECT ObjectID2 FROM Permissions WHERE ObjectTypeID=99 AND (PersonID = @UserID OR PersonID IN 
	(SELECT UserID FROM Proxies WHERE ProxyID = @UserID))))
	OR
	(SharedFolderID > 0)))
	OR
	(s.ID2 = ANY (SELECT ObjectID2 FROM Permissions p WHERE p.ObjectTypeID=99 AND p.LastEditTime > @LastSyncDown AND
	(p.PersonID = @UserID OR p.PersonID IN 
	(SELECT UserID FROM Proxies WHERE ProxyID = @UserID))));
	RETURN
GO
69.ALTER PROCEDURE [dbo].[spUserSyncDownGetVariableSets] 
	(
		@UserID int,
		@LastSyncDown datetime
	)
	AS
	SELECT * FROM VariableSets v
	WHERE (v.LastEditTime > @LastSyncDown AND 
	((v.OwnerID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID)) 
	OR 
	(v.OwnerID1 = @UserID) 
	OR 
	(v.ID2 = ANY (SELECT ObjectID2 FROM Permissions WHERE ObjectTypeID=104 AND (PersonID = @UserID OR PersonID IN 
	(SELECT UserID FROM Proxies WHERE ProxyID = @UserID))))
	OR
	(v.SharedFolderID > 0)))
	OR
	(v.ID2 = ANY (SELECT ObjectID2 FROM Permissions p WHERE p.ObjectTypeID=104 AND p.LastEditTime > @LastSyncDown AND
	(p.PersonID = @UserID OR p.PersonID IN 
	(SELECT UserID FROM Proxies WHERE ProxyID = @UserID))));
	RETURN
GO
70.ALTER PROCEDURE [dbo].[spSyncUpAssignmentsUpdate]
	AS
	UPDATE Assignments
	SET LastEditTime = s.LastEditTime
	FROM Assignments AS a INNER JOIN SyncAssignmentsTmp AS s ON s.TargetObjectTypeID = a.TargetObjectTypeID AND s.TargetObjectID = a.TargetObjectID 
	AND s.AssignedObjectTypeID = a.AssignedObjectTypeID AND s.AssignedObjectID = a.AssignedObjectID
	WHERE s.LastEditTime > a.LastEditTime;
	RETURN
GO
71.CREATE PROCEDURE [dbo].[spCountriesDelete]
	( 
		@ID int
	)
	AS
	DELETE FROM Countries WHERE ID=@ID;
GO
72.CREATE PROCEDURE [dbo].[spStatesDelete]
	( 
		@ID int
	)
	AS
	DELETE FROM States WHERE ID=@ID;
GO
73.CREATE PROCEDURE [dbo].[spCountiesDelete]
	( 
		@ID int
	)
	AS
	DELETE FROM Counties WHERE ID=@ID;
GO
74.ALTER PROCEDURE [dbo].[spSyncUpCountriesUpdate]
	AS
	UPDATE    Countries
	SET       [Name] = s.Name, ISONum = s.ISONum, LastEditTime = s.LastEditTime
	FROM Countries INNER JOIN SyncCountriesTmp AS s ON Countries.ID = s.ID
	WHERE s.LastEditTime > Countries.LastEditTime
	RETURN
GO
75.ALTER PROCEDURE [dbo].[spUserSyncDownGetPermissions]
	(
		@UserID int,
		@LastSyncDown datetime
	)
	AS
	SELECT * FROM Permissions p 
	WHERE p.LastEditTime > @LastSyncDown AND ((p.PersonID = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID) OR (p.PersonID = @UserID))  
	OR (p.PersonID = ANY (SELECT ID1 FROM People WHERE ID2=0 AND (OwnerID = @UserID OR LinkedPersonID = @UserID))) OR
	(p.ObjectID1 = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID) OR (p.ObjectID1 = @UserID))  
	OR (p.ObjectID1 = ANY (SELECT ID1 FROM People WHERE ID2=0 AND (OwnerID = @UserID OR LinkedPersonID = @UserID))));
	RETURN
GO
76.CREATE PROCEDURE [dbo].[spKeySetsDeleteByObjectIDs] 
	(
		@ScopeID1 int, 
		@ScopeID2 int
	)
	AS
	DELETE FROM KeySets 
	WHERE ScopeID1=@ScopeID1 and ScopeID2=@ScopeID2;
GO
77.ALTER PROCEDURE [dbo].[spUserSyncDownGetGroupAssignments]
	(
		@UserID int,
		@LastSyncDown datetime
	)
AS
	SELECT * FROM GroupAssignments p 
	WHERE ((p.PersonID = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID AND LastEditTime > @LastSyncDown))
	OR (p.PersonID = ANY (SELECT UserID FROM Proxies WHERE ProxyID = @UserID) AND p.LastEditTime > @LastSyncDown) 
	OR (p.PersonID = @UserID AND p.LastEditTime > @LastSyncDown))
	RETURN
GO