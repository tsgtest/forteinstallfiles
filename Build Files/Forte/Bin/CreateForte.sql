USE [master]
GO
CREATE DATABASE [mp10]
 COLLATE SQL_Latin1_General_CP1_CI_AS
GO
/*=======================================================
Uncomment line below to set specific compatibility level
	90=SQL 2005
	100=SQL 2008
	110=SQL 2012
	120=SQL 2014
=========================================================*/
/*EXEC dbo.sp_dbcmptlevel @dbname=N'mp10', @new_cmptlevel=90*/
